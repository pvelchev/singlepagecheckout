<?php
/**
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
 */
class SinglepagecheckoutiwCheckoutModuleFrontController extends ModuleFrontController
{
    private $reloadSubsequentSections = false;
    private $steps = array();
    private $stepId = 0;
    private $default_lang;
    private $ajaxResponseStatus = 'ok';
    private $step_1_display;
    private $step_2_display;
    private $step_3_display;
    private $step_4_display;
    private $updateToken = false;
    public $guestAllowed;
    private $displayFinalSummary;
    private $isUserLogged;
    private $color_scheme;
    private $color_scheme_items_prefix = 'spciw_color_scheme_';
    private $custom_template_vars = array();
    private $displaySigninForm = false;
    private $page_errors = array();
    private $form_errors = array();
    private $use_same_address = true;
    private $display_delivery_title = true;
    private $display_invoice_title = true;
    private $display_cancel_button = true;
    private $addNewAddress = false;
    private $addNewAddressAsInvoice = false;
    private $is_delivery_address = 1;
    private $saveAddressData = array();
    private $addressId = 0;
    private $countryId = 0;
    private $customerAddresses;
    private $deliveryOptionsFinder;
    private $cartsummary_errors;
    public function init()
    {
        parent::init();
    }
    public function initContent()
    {
        parent::initContent();
        $this->initLayout();
        $this->initLanguage();
        $this->initUserLoginStatus();
        $this->initDisplaySteps();
        $this->initSteps();
        $this->initGuestAllowed();
        $this->initDisplayFinalSummary();
        $this->initColorScheme();
    }
    public function postProcess()
    {
        parent::postProcess();
    }
    private function initColorScheme()
    {
        require_once(__DIR__ . '/../../classes/ThemeColors.php');
        $source                       = ThemeColors::getDefaultSource();
        $use_the_default_color_scheme = (int) Configuration::get('spciw_use_the_default_color_scheme');
        if ($use_the_default_color_scheme == 0) {
            foreach ($source as $name => $default_color) {
                $prop_name = $this->color_scheme_items_prefix . $name;
                $value     = Configuration::get($prop_name);
                if ($value) {
                    $source[$name] = $value;
                }
            }
        }
        $this->color_scheme = $source;
    }
    private function initPercCompleted()
    {
        $step_completed = array();
        if ($this->displayAddressSection()) {
            $step_completed[] = 1;
            if ($this->isDeliveryAddressSelected()) {
                $step_completed[] = 1;
                if (!$this->isCartVirtual()) {
                    $this->stepShippingMethods_initDeliveryOptionsFinder();
                    $step_completed[] = count($this->deliveryOptionsFinder->getDeliveryOptions()) > 0 && !empty(current($this->context->cart->getDeliveryOption(null, false, false))) ? 1 : 0;
                }
            } else {
                $step_completed[] = 0;
                $step_completed[] = 0;
            }
        } else {
            $step_completed[] = 0;
            $step_completed[] = 0;
            if (!$this->isCartVirtual()) {
                $step_completed[] = 0;
            }
        }
        $step_completed[] = 0;
        $count            = count($step_completed);
        $onep             = 100 / $count;
        $perc_completed   = 0;
        foreach ($step_completed as $v) {
            if ($v == 1) {
                $perc_completed += $onep;
            }
        }
        $perc_completed = ceil($perc_completed);
        if ($perc_completed > 100) {
            $perc_completed = 100;
        }
        return array(
            'perc_completed' => $perc_completed,
            'steps_status' => implode($step_completed, ',')
        );
    }
    private function initUserLoginStatus()
    {
        $this->isUserLogged = $this->context->customer->isLogged($this->guestAllowed == 1 ? true : false);
    }
    private function initDisplayFinalSummary()
    {
        $this->displayFinalSummary = (int) Configuration::get('PS_FINAL_SUMMARY_ENABLED');
    }
    public function initGuestAllowed()
    {
        $this->guestAllowed = (int) Configuration::get('PS_GUEST_CHECKOUT_ENABLED');
    }
    public function initLanguage()
    {
        $this->default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
    }
    private function isCheckoutCompleted()
    {
        $result = false;
        $this->stepShippingMethods_initDeliveryOptionsFinder();
        if ($this->isCartVirtual()) {
            $delivery = true;
        } else {
            $delivery = count($this->deliveryOptionsFinder->getDeliveryOptions()) > 0 && !empty(current($this->context->cart->getDeliveryOption(null, false, false)));
        }
        if ($this->isDeliveryAddressSelected() && ($this->isUserLogged || $this->isUserGuest()) && $delivery) {
            $result = true;
        }
        return $result;
    }
    private function initDisplaySteps()
    {
        $this->step_1_display = true;
        $this->step_2_display = $this->isUserLogged;
        $this->step_3_display = false;
        if ($this->step_2_display) {
            if (count($this->context->customer->getSimpleAddresses()) > 0) {
                $this->step_3_display = true;
            }
        }
        $this->step_4_display = false;
        if ($this->step_3_display) {
            $paymentOptionsFinder = new PaymentOptionsFinder();
            $payment_options      = $paymentOptionsFinder->present();
            if (!empty($payment_options) && count($payment_options) > 0) {
                $this->step_4_display = true;
            }
        }
    }
    private function getAddressOptionalFormFieldsVisibility()
    {
        $return = array(
            'display_field_company' => (int) Configuration::get('spciw_display_optional_form_field_company'),
            'display_field_vat_number' => (int) Configuration::get('spciw_display_optional_form_field_vat_number'),
            'display_field_address_complement' => (int) Configuration::get('spciw_display_optional_form_field_address_complement'),
            'display_field_phone' => (int) Configuration::get('spciw_display_optional_form_field_phone')
        );
        return $return;
    }
    private function getUserOptionalFormFieldsVisibility()
    {
        $return = array(
            'display_field_birthday' => (int) Configuration::get('spciw_display_optional_form_field_birthday')
        );
        return $return;
    }
    private function addUserOptionalFormFieldsVisibility(&$container)
    {
        $optionalFormFields = $this->getUserOptionalFormFieldsVisibility();
        $container          = array_merge($container, $optionalFormFields);
    }
    private function addAddressOptionalFormFieldsVisibility(&$container)
    {
        $optionalFormFields = $this->getAddressOptionalFormFieldsVisibility();
        $container          = array_merge($container, $optionalFormFields);
    }
    private function getProgressBarVisibility()
    {
        return (int) Configuration::get('spciw_display_progress_bar');
    }
    private function getReassuranceSectionVisibility()
    {
        return (int) Configuration::get('spciw_display_reassurance_block');
    }
    private function getlLast2StepsAsInfoVisibility()
    {
        return (int) Configuration::get('spciw_display_the_content_of_last_2_steps_as_info');
    }
    private function getlLast3StepsVisibility()
    {
        return (int) Configuration::get('spciw_hide_last_3_checkout_steps');
    }
    protected function stepAddresses_setIdAddressDelivery($id_address)
    {
        $this->context->cart->updateAddressId($this->context->cart->id_address_delivery, $id_address);
        $this->context->cart->id_address_delivery = $id_address;
        $this->context->cart->save();
    }
    protected function stepAddresses_setIdAddressInvoice($id_address)
    {
        $this->context->cart->id_address_invoice = $id_address;
        $this->context->cart->save();
    }
    private function getUseSameAddress()
    {
        if (is_numeric($this->context->cart->id_address_delivery) && is_numeric($this->context->cart->id_address_invoice)) {
            if ($this->context->cart->id_address_delivery == $this->context->cart->id_address_invoice) {
                $this->use_same_address = true;
            } else {
                $this->use_same_address = false;
            }
        }
        return $this->use_same_address;
    }
    private function setAjaxResponseStatus($status_name)
    {
        switch ($status_name) {
            case 'err';
                $this->ajaxResponseStatus = 'err';
                break;
            case 'ok':
            default:
                $this->ajaxResponseStatus = 'ok';
                break;
        }
    }
    private function prepareTempalteVars($data)
    {
        $templateVars = array_merge($data, array(
            'form_errors' => $this->form_errors
        ), array(
            'page_errors' => $this->page_errors
        ), $this->custom_template_vars);
        return $templateVars;
    }
    private function getNextStepId()
    {
        return ++$this->stepId;
    }
    private function initLayout()
    {
        $layoutId = (int) Configuration::get('spciw_select_page_layout');
        switch ($layoutId) {
            case 2:
                $layoutName = 'Pizza';
                break;
            case 3:
                $layoutName = 'Piperony';
                break;
            case 4:
                $layoutName = 'Slice';
                break;
            case 5:
                $layoutName = 'Domino';
                break;
            case 1:
            default:
                $layoutName = 'Classic';
                break;
        }
        $this->layoutName = $layoutName;
    }
    private function isAddressSelected($address_id, $refresh)
    {
        if ($refresh || empty($this->customerAddresses)) {
            $this->customerAddresses = $this->context->customer->getSimpleAddresses($this->context->language->id, true);
        }
        $result = false;
        if (1 !== preg_match('/^\d+$/', $address_id)) {
            return $result;
        }
        foreach ($this->customerAddresses as $address) {
            if (strval($address["id"]) == strval($address_id)) {
                $result = true;
                break;
            }
        }
        return $result;
    }
    private function isDeliveryAddressSelected($refresh = false)
    {
        return $this->isAddressSelected($this->context->cart->id_address_delivery, $refresh);
    }
    private function isInvoiceAddressSelected($refresh = false)
    {
        return $this->isAddressSelected($this->context->cart->id_address_invoice, $refresh);
    }
    private function isUserGuest()
    {
        return $this->context->customer->is_guest;
    }
    private function displayAddressSection()
    {
        if ($this->isUserLogged || $this->isUserGuest()) {
            return true;
        }
        return false;
    }
    private function isCartVirtual()
    {
        return $this->context->cart->isVirtualCart();
    }
    private function ajaxResponse($response_data)
    {
        $this->ajaxDie(Tools::jsonEncode($response_data));
    }
    private function initSteps()
    {
        $this->stepId = 0;
        $steps        = array();
        $steps[1]     = array(
            'id' => $this->getNextStepId(),
            'id_name' => 'PersonalInformation',
            'name' => $this->l('Personal Information')
        );
        $steps[2]     = array(
            'id' => $this->getNextStepId(),
            'id_name' => 'CustomerAddresses',
            'name' => $this->l('Addresses')
        );
        $steps[3]     = array();
        if (!$this->isCartVirtual()) {
            $steps[3] = array(
                'id' => $this->getNextStepId(),
                'id_name' => 'ShippingMethods',
                'name' => $this->l('Shipping Methods')
            );
        }
        $steps[4]    = array(
            'id' => $this->getNextStepId(),
            'id_name' => 'PaymentMethods',
            'name' => $this->l('Payment Methods')
        );
        $this->steps = $steps;
    }
    public function displayAjaxLoadBaseLayout()
    {
        $last3StepsVisibility       = $this->getlLast3StepsVisibility();
        $last2StepsAsInfoVisibility = $this->getlLast2StepsAsInfoVisibility();
        $display_progress_bar       = $this->getProgressBarVisibility();
        $template_vars              = array(
            'checkout_steps' => $this->steps,
            'layout_name' => $this->layoutName,
            'hide_last_3_steps' => $last3StepsVisibility,
            'display_reassurance_block' => $this->getReassuranceSectionVisibility(),
            'display_progress_bar' => $display_progress_bar,
            'color_scheme' => $this->color_scheme
        );
        if ($display_progress_bar == 1) {
            $template_vars['progress_bar'] = $this->initPercCompleted();
        }
        $this->context->smarty->assign($template_vars);
        $content = $this->context->smarty->fetch("module:singlepagecheckoutiw/views/templates/front/layouts/{$this->layoutName}/layout.tpl");
        $this->ajaxResponse(array(
            'status' => 'ok',
            'data' => array(
                'content' => $content,
                'steps' => array_merge($this->steps, array(
                    array(
                        'id_name' => 'CartSummary'
                    )
                )),
                'settings' => array(
                    'hide_last_3_steps' => $last3StepsVisibility,
                    'display_as_info' => $last2StepsAsInfoVisibility,
                    'steps' => $this->steps,
                    'display_progress_bar' => $this->getProgressBarVisibility(),
                    'is_virtual' => $this->isCartVirtual()
                )
            )
        ));
    }
    private function getCustomerPersonalInformation()
    {
        $data              = array();
        $data['firstname'] = $this->context->customer->firstname;
        $data['lastname']  = $this->context->customer->lastname;
        return $data;
    }
    private function stepPersonalInformation_getContent()
    {
        $data = array();
        if ($this->isUserLogged) {
            $template = 'personal_info_as_user';
            $data     = array(
                'customer' => $this->getCustomerPersonalInformation()
            );
        } else {
            $template                  = 'personal_info_as_guest';
            $data['displaySigninForm'] = $this->displaySigninForm;
            $data['guestAllowed']      = $this->guestAllowed;
            $this->addUserOptionalFormFieldsVisibility($data);
        }
        return array(
            'template' => $template,
            'data' => $data
        );
    }
    public function displayAjaxLoadPersonalInformation()
    {
        $section_content = $this->stepPersonalInformation_getContent();
        $this->context->smarty->assign($this->prepareTempalteVars($section_content['data']));
        $content  = $this->context->smarty->fetch("module:singlepagecheckoutiw/views/templates/front/layouts/{$this->layoutName}/{$section_content['template']}.tpl");
        $response = array(
            'status' => $this->ajaxResponseStatus,
            'data' => array(
                'PersonalInformation' => array(
                    'content' => $content
                )
            ),
            'reload' => $this->reloadSubsequentSections
        );
        if ($this->updateToken) {
            $response['token'] = Tools::getToken();
        }
        $this->ajaxResponse($response);
    }
    public function displayAjaxUserSignIn()
    {
        $request_data = Tools::getAllValues();
        $loginForm    = $this->makeLoginForm();
        $loginForm->fillWith($request_data);
        if ($loginForm->submit()) {
            $this->displaySigninForm        = false;
            $this->reloadSubsequentSections = true;
            $this->updateToken              = true;
            $this->initUserLoginStatus();
        } else {
            $this->displaySigninForm    = true;
            $this->page_errors[]        = $this->l('Authentication failed.');
            $this->custom_template_vars = array(
                'signin_email' => $request_data['email']
            );
        }
        $this->displayAjaxLoadPersonalInformation();
    }
    public function displayAjaxUserOrderAsGuest()
    {
        $request_data = Tools::getAllValues();
        $registerForm = $this->makeCustomerForm();
        $registerForm->fillWith($request_data);
        if (!$registerForm->submit()) {
            $this->form_errors          = $registerForm->getErrors();
            $this->custom_template_vars = array(
                'customer_gender' => $request_data['id_gender'],
                'customer_firstname' => $request_data['firstname'],
                'customer_lastname' => $request_data['lastname'],
                'customer_email' => $request_data['email']
            );
        } else {
            $this->reloadSubsequentSections = true;
            $this->updateToken              = true;
            $this->initUserLoginStatus();
        }
        $this->displayAjaxLoadPersonalInformation();
    }
    private function stepAddresses_fillAddressForm(&$data)
    {
        $this->addAddressOptionalFormFieldsVisibility($data);
        $data['address_country_selected'] = 0;
        $data['address_state_selected']   = 0;
        $data['address_id']               = $this->addressId;
        $isSaveAddressWithErrors          = empty($this->saveAddressData);
        if ($this->addressId == 0 && $this->countryId == 0 && $isSaveAddressWithErrors) {
            return;
        }
        if ($this->addressId == 0 && $this->countryId > 0) {
            $data['address_country_selected'] = $this->countryId;
            if ($data['address_countries'][$this->countryId]['contains_states'] != '0' && !empty($data['address_countries'][$this->countryId]['states'])) {
                $data['address_states'] = $data['address_countries'][$this->countryId]['states'];
            }
            return;
        }
        if ($isSaveAddressWithErrors) {
            $address = new Address($this->addressId);
            if ($address->deleted == '1') {
                return;
            }
            $data['address_firstname']          = $address->firstname;
            $data['address_lastname']           = $address->lastname;
            $data['address_company']            = $address->company;
            $data['address_vat_number']         = $address->vat_number;
            $data['address_address']            = $address->address1;
            $data['address_address_complement'] = $address->address2;
            $data['address_zip_postal_code']    = $address->postcode;
            $data['address_city']               = $address->city;
            $data['address_phone']              = $address->phone;
            $address_id_country                 = $address->id_country;
            $address_id_state                   = $address->id_state;
        } else {
            $data['address_firstname']          = $this->saveAddressData['firstname'];
            $data['address_lastname']           = $this->saveAddressData['lastname'];
            $data['address_company']            = $this->saveAddressData['company'];
            $data['address_vat_number']         = $this->saveAddressData['vat_number'];
            $data['address_address']            = $this->saveAddressData['address1'];
            $data['address_address_complement'] = $this->saveAddressData['address2'];
            $data['address_zip_postal_code']    = $this->saveAddressData['postcode'];
            $data['address_city']               = $this->saveAddressData['city'];
            $data['address_phone']              = $this->saveAddressData['phone'];
            $address_id_country                 = $this->saveAddressData['id_country'];
            $address_id_state                   = empty($this->saveAddressData['id_state']) ? '' : $this->saveAddressData['id_state'];
        }
        if ($this->countryId == 0) {
            $data['address_country_selected'] = $address_id_country;
            $data['address_state_selected']   = $address_id_state;
            if (!empty($address_id_state)) {
                $data['address_states'] = $data['address_countries'][$address_id_country]['states'];
            }
        } else {
            $data['address_country_selected'] = $this->countryId;
            if ($data['address_countries'][$this->countryId]['contains_states'] != '0' && !empty($data['address_countries'][$this->countryId]['states'])) {
                $data['address_states'] = $data['address_countries'][$this->countryId]['states'];
            }
        }
    }
    private function stepAddresses_setupAddressForm(&$data, &$template)
    {
        $template                      = 'addresses_form';
        $data['use_same_address']      = $this->use_same_address;
        $data['display_cancel_button'] = $this->display_cancel_button;
        $data['is_delivery_address']   = $this->is_delivery_address;
        $counrties                     = Country::getCountries($this->default_lang);
        $data['address_countries']     = $counrties;
        $this->stepAddresses_fillAddressForm($data);
    }
    public function stepAddresses_setupAddressList(&$data, &$template)
    {
        $template                       = 'addresses_list';
        $data['use_same_address']       = $this->use_same_address;
        $data['display_delivery_title'] = $this->display_delivery_title;
        $data['display_invoice_title']  = $this->display_delivery_title;
    }
    private function stepAddresses_getContent()
    {
        $data     = array();
        $template = NULL;
        if ($this->isUserLogged) {
            $addresses = $this->context->customer->getAddresses($this->default_lang);
            if (empty($addresses) && $this->countryId == 0 && empty($this->saveAddressData)) {
                $this->use_same_address       = true;
                $this->display_delivery_title = false;
                $this->display_cancel_button  = false;
                $data['customer_id']          = $this->context->customer->id;
                $this->stepAddresses_setupAddressForm($data, $template);
            } else if ($this->countryId > 0) {
                if (count($addresses)) {
                    $this->use_same_address = true;
                } else {
                    $this->use_same_address = false;
                }
                $this->display_delivery_title = false;
                if (count($addresses) > 0) {
                    $this->display_cancel_button = true;
                } else {
                    $this->display_cancel_button = false;
                }
                $data['customer_id'] = $this->context->customer->id;
                $this->stepAddresses_setupAddressForm($data, $template);
            } else if ($this->addressId > 0 || $this->addNewAddress) {
                $this->use_same_address       = true;
                $this->display_delivery_title = false;
                $this->display_cancel_button  = true;
                if ($this->addNewAddress) {
                    if ($this->context->cart->id_address_delivery != $this->context->cart->id_address_invoice) {
                        $this->use_same_address = false;
                    }
                }
                if ($this->addNewAddressAsInvoice) {
                    $this->is_delivery_address = 0;
                }
                $data['customer_id'] = $this->context->customer->id;
                $this->stepAddresses_setupAddressForm($data, $template);
            } else if (!empty($this->saveAddressData)) {
                $this->use_same_address       = false;
                $this->display_delivery_title = false;
                if (count($addresses) > 0) {
                    $this->display_cancel_button = true;
                } else {
                    $this->display_cancel_button = false;
                }
                if ($this->addNewAddressAsInvoice) {
                    $this->is_delivery_address = 0;
                }
                $data['customer_id'] = $this->context->customer->id;
                $this->stepAddresses_setupAddressForm($data, $template);
            } else {
                $this->use_same_address = $this->getUseSameAddress();
                if ($this->use_same_address) {
                    $this->display_delivery_title = false;
                    $this->display_invoice_title  = false;
                } else {
                    $this->display_delivery_title = true;
                    $this->display_invoice_title  = true;
                }
                $data['display_add_new_address']            = true;
                $data['customer_delivery_address_selected'] = $this->context->cart->id_address_delivery;
                $data['customer_invoice_address_selected']  = $this->context->cart->id_address_invoice;
                $data['customer_addresses']                 = array();
                foreach ($addresses as $address) {
                    $obj                          = new Address((int) $address['id_address']);
                    $formated_address             = AddressFormat::generateAddress($obj, array(), '<br>');
                    $address['id']                = $address['id_address'];
                    $address['formatted']         = $formated_address;
                    $data['customer_addresses'][] = $address;
                }
                $this->stepAddresses_setupAddressList($data, $template);
            }
        } else {
            $template        = 'complete_previous_step';
            $data['step_id'] = 1;
        }
        $data['cart'] = $this->cart_presenter->present($this->context->cart);
        return array(
            'template' => $template,
            'data' => $data
        );
    }
    public function displayAjaxLoadCustomerAddresses()
    {
        $last3StepsVisibility = $this->getlLast3StepsVisibility();
        if ($this->step_2_display || ((!$this->step_2_display) && $last3StepsVisibility == 0)) {
            $section_content = $this->stepAddresses_getContent();
            $this->context->smarty->assign($this->prepareTempalteVars($section_content['data']));
            $content = $this->context->smarty->fetch("module:singlepagecheckoutiw/views/templates/front/layouts/{$this->layoutName}/{$section_content['template']}.tpl");
        } else {
            $content = '';
        }
        $this->ajaxResponse(array(
            'status' => $this->ajaxResponseStatus,
            'data' => array(
                'CustomerAddresses' => array(
                    'content' => $content
                )
            ),
            'reload' => $this->reloadSubsequentSections
        ));
    }
    public function displayAjaxChangeCountry()
    {
        $id_country = Tools::getValue('id_country');
        if ($id_country) {
            $this->countryId = (int) $id_country;
        }
        $this->displayAjaxLoadCustomerAddresses();
    }
    public function displayAjaxSaveAddress()
    {
        $requestParams = Tools::getAllValues();
        if (isset($requestParams['id_customer']) && $requestParams['id_customer'] == $this->context->customer->id) {
            $addressForm  = $this->makeAddressForm();
            $addressSaved = $addressForm->fillWith($requestParams)->submit();
            if ($addressSaved) {
                $all_customer_addresses    = array_values($this->context->customer->getSimpleAddresses($this->context->language->id));
                $total_amount_of_addresses = count($all_customer_addresses);
                if ($requestParams['is_delivery_address'] == '0') {
                    if ($total_amount_of_addresses == 2) {
                        foreach ($all_customer_addresses as $address) {
                            if ($address['id'] != $this->context->cart->id_address_delivery) {
                                $this->stepAddresses_setIdAddressInvoice($address['id']);
                                break;
                            }
                        }
                    }
                }
                if ($total_amount_of_addresses == 1) {
                    $this->stepAddresses_setIdAddressDelivery($all_customer_addresses[0]['id']);
                    $this->stepAddresses_setIdAddressInvoice($all_customer_addresses[0]['id']);
                }
                $this->reloadSubsequentSections = true;
            } else {
                $this->form_errors     = $addressForm->getErrors();
                $this->saveAddressData = $requestParams;
            }
        }
        $this->displayAjaxLoadCustomerAddresses();
    }
    public function displayAjaxEditAddress()
    {
        if (Tools::getIsset("address_id")) {
            $addressId = (int) Tools::getValue("address_id");
            $a         = new Address($addressId);
            if ($a->id_customer == $this->context->customer->id) {
                $this->addressId = $addressId;
            }
        }
        $this->displayAjaxLoadCustomerAddresses();
    }
    public function displayAjaxDeleteAddress()
    {
        if (Tools::getIsset("address_id") && Tools::getIsset("token")) {
            $address_id               = Tools::getValue("address_id");
            $current_delivery_address = $this->context->cart->id_address_delivery;
            $current_invoice_address  = $this->context->cart->id_address_invoice;
            $address_persister        = new CustomerAddressPersister($this->context->customer, $this->context->cart, Tools::getToken());
            if ($address_persister->delete(new Address($address_id, $this->context->language->id), Tools::getValue("token"))) {
                $all_customer_addresses = array_values($this->context->customer->getSimpleAddresses($this->context->language->id));
                if (count($all_customer_addresses) > 0) {
                    $this->stepAddresses_setIdAddressDelivery($all_customer_addresses[0]['id']);
                    $this->stepAddresses_setIdAddressInvoice($all_customer_addresses[0]['id']);
                }
                $this->reloadSubsequentSections = true;
            } else {
                $this->page_errors[] = $this->getTranslator()->trans('Could not delete address.', array(), 'Shop.Notifications.Error');
            }
        }
        $this->displayAjaxLoadCustomerAddresses();
    }
    public function displayAjaxAddNewAddress()
    {
        $this->addNewAddress = true;
        $this->displayAjaxLoadCustomerAddresses();
    }
    public function displayAjaxDifferentInvoiceAddress()
    {
        $all_customer_addresses = array_values($this->context->customer->getSimpleAddresses($this->context->language->id));
        $total_addresses        = count($all_customer_addresses);
        if ($total_addresses > 1) {
            foreach ($all_customer_addresses as $address) {
                if ($address['id'] != $this->context->cart->id_address_delivery) {
                    $this->stepAddresses_setIdAddressInvoice($address['id']);
                    break;
                }
            }
        } else if ($total_addresses == 1) {
            $this->addNewAddress          = true;
            $this->addNewAddressAsInvoice = true;
        }
        $this->displayAjaxLoadCustomerAddresses();
    }
    public function displayAjaxSetAddressId()
    {
        if (Tools::getIsset("address_id") && Tools::getIsset("address_type") && Tools::getIsset("token")) {
            $address_id   = Tools::getValue("address_id");
            $address_type = Tools::getValue("address_type");
            $token        = Tools::getValue("token");
            if ($token == Tools::getToken()) {
                if ($address_type == 'delivery') {
                    $this->stepAddresses_setIdAddressDelivery($address_id);
                    $this->reloadSubsequentSections = true;
                } else {
                    $this->stepAddresses_setIdAddressInvoice($address_id);
                }
            }
        }
        $this->displayAjaxLoadCustomerAddresses();
    }
    public function displayAjaxShippingAddressSameAsBilling()
    {
        $this->stepAddresses_setIdAddressInvoice($this->context->cart->id_address_delivery);
        $this->displayAjaxLoadCustomerAddresses();
    }
    public function stepShippingMethods_getGiftCostForLabel()
    {
        $include_taxes       = (Product::getTaxCalculationMethod((int) $this->context->cart->id_customer) && (int) Configuration::get('PS_TAX'));
        $display_taxes_label = (Configuration::get('PS_TAX') && !Configuration::get('AEUC_LABEL_TAX_INC_EXC'));
        $gift_cost           = $this->context->cart->getGiftWrappingPrice($include_taxes);
        if ($gift_cost != 0) {
            $tax_label       = '';
            $price_formatter = new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter();
            if ($include_taxes && $this->getDisplayTaxesLabel()) {
                $tax_label .= ' tax incl.';
            } elseif ($this->getDisplayTaxesLabel()) {
                $tax_label .= ' tax excl.';
            }
            return sprintf($this->getTranslator()->trans(' (additional cost of %giftcost% %taxlabel%)', array(
                '%giftcost%' => $price_formatter->convertAndFormat($gift_cost),
                '%taxlabel%' => $tax_label
            ), 'Shop.Theme.Checkout'));
        }
        return '';
    }
    private function stepShippingMethods_initDeliveryOptionsFinder($refresh = false)
    {
        if ($refresh == false && $this->deliveryOptionsFinder instanceof DeliveryOptionsFinder) {
            return;
        }
        $this->deliveryOptionsFinder = new DeliveryOptionsFinder($this->context, $this->getTranslator(), $this->objectPresenter, new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter());
    }
    public function stepShippingMethods_getContent()
    {
        $last2StepsAsInfoVisibility = $this->getlLast2StepsAsInfoVisibility();
        $display_as_info            = false;
        if ($this->step_3_display || $last2StepsAsInfoVisibility == 1) {
            $this->stepShippingMethods_initDeliveryOptionsFinder(true);
            $template                         = 'shipping_methods';
            $data                             = array();
            $data['hookDisplayBeforeCarrier'] = Hook::exec('displayBeforeCarrier', array(
                'cart' => $this->context->cart
            ));
            $data['hookDisplayAfterCarrier']  = Hook::exec('displayAfterCarrier', array(
                'cart' => $this->context->cart
            ));
            $data['id_address']               = $this->context->cart->id_address_delivery;
            $data['delivery_options']         = $this->deliveryOptionsFinder->getDeliveryOptions();
            $data['delivery_option']          = current($this->context->cart->getDeliveryOption(null, false, false));
            $data['recyclable']               = $this->context->cart->recyclable;
            $data['recyclablePackAllowed']    = (bool) Configuration::get('PS_RECYCLABLE_PACK');
            $data['gift']                     = array(
                'allowed' => (bool) Configuration::get('PS_GIFT_WRAPPING'),
                'isGift' => $this->context->cart->gift,
                'label' => $this->getTranslator()->trans('I would like my order to be gift wrapped' . $this->stepShippingMethods_getGiftCostForLabel(), array(), 'Checkout'),
                'message' => $this->context->cart->gift_message
            );
            if ((!$this->step_3_display) && $last2StepsAsInfoVisibility == 1) {
                $display_as_info = true;
            }
        } else {
            $template        = 'complete_previous_step';
            $data['step_id'] = 2;
        }
        $data['display_as_info'] = $display_as_info;
        return array(
            'data' => $data,
            'template' => $template
        );
    }
    public function displayAjaxLoadShippingMethods()
    {
        $last3StepsVisibility       = $this->getlLast3StepsVisibility();
        $last2StepsAsInfoVisibility = $this->getlLast2StepsAsInfoVisibility();
        if ($this->step_3_display || ((!$this->step_3_display) && $last3StepsVisibility == 0)) {
            $section_content = $this->stepShippingMethods_getContent();
            $this->context->smarty->assign($this->prepareTempalteVars($section_content['data']));
            $content = $this->context->smarty->fetch("module:singlepagecheckoutiw/views/templates/front/layouts/{$this->layoutName}/{$section_content['template']}.tpl");
        } else {
            $content = '';
        }
        $response_data = array(
            'content' => $content
        );
        if ((!$this->step_3_display) && $last2StepsAsInfoVisibility == 1) {
            $response_data['display_as_info'] = true;
        } else {
            $response_data['display_as_info'] = false;
        }
        $this->ajaxResponse(array(
            'status' => $this->ajaxResponseStatus,
            'data' => array(
                'ShippingMethods' => $response_data
            ),
            'reload' => $this->reloadSubsequentSections
        ));
    }
    public function displayAjaxUpdateShippingMethod()
    {
        if (Tools::getIsset("delivery_address_id") && Tools::getIsset("carrier_id")) {
            $delivery_address_id = intval(Tools::getValue("delivery_address_id"));
            $carrier_id          = strval(Tools::getValue("carrier_id"));
            $this->stepShippingMethods_setDeliveryOption(array(
                $delivery_address_id => $carrier_id
            ));
            if (Tools::getIsset("recyclable")) {
                $this->stepShippingMethods_setRecyclable(Tools::getValue("recyclable"));
            }
            if (Tools::getIsset("gift") && Tools::getIsset("gift_message")) {
                $gift         = Tools::getValue("gift") == '0' ? false : true;
                $gift_message = strval(Tools::getValue("gift_message"));
                $this->stepShippingMethods_setGift($gift, $gift_message);
            }
            if (Tools::getIsset("delivery_message")) {
                $delivery_message = strval(Tools::getValue("delivery_message"));
                $this->stepShippingMethods_setMessage($delivery_message);
            }
            $this->context->cart->update();
            Hook::exec('actionCarrierProcess', array(
                'cart' => $this->context->cart
            ));
            $this->reloadSubsequentSections = true;
        }
        $this->ajaxResponse(array(
            'status' => $this->ajaxResponseStatus,
            'data' => array(
                'ShippingMethods' => array()
            ),
            'reload' => $this->reloadSubsequentSections
        ));
    }
    private function stepShippingMethods_setDeliveryOption($option)
    {
        $this->context->cart->setDeliveryOption($option);
    }
    private function stepShippingMethods_setRecyclable($option)
    {
        $this->context->cart->recyclable = (int) $option;
    }
    private function stepShippingMethods_setGift($gift, $gift_message)
    {
        $this->context->cart->gift         = (int) $gift;
        $this->context->cart->gift_message = $gift_message;
    }
    private function stepShippingMethods_setMessage($messageContent)
    {
        $messageContent = Tools::safeOutput($messageContent);
        if ($messageContent) {
            if ($oldMessage = Message::getMessageByCartId((int) $this->context->cart->id)) {
                $message          = new Message((int) $oldMessage['id_message']);
                $message->message = $messageContent;
                $message->update();
            } else {
                $message              = new Message();
                $message->message     = $messageContent;
                $message->id_cart     = (int) $this->context->cart->id;
                $message->id_customer = (int) $this->context->cart->id_customer;
                $message->add();
            }
        } else {
            if ($oldMessage = Message::getMessageByCartId($this->context->cart->id)) {
                $message = new Message($oldMessage['id_message']);
                $message->delete();
            }
        }
    }
    private function stepPaymentMethods_getContent()
    {
        $last2StepsAsInfoVisibility = $this->getlLast2StepsAsInfoVisibility();
        $display_as_info            = false;
        if ($this->step_4_display || $last2StepsAsInfoVisibility == 1) {
            $template                  = 'payment_methods';
            $data                      = array();
            $paymentOptionsFinder      = new PaymentOptionsFinder();
            $conditionsToApproveFinder = new ConditionsToApproveFinder($this->context, $this->getTranslator());
            $this->stepShippingMethods_initDeliveryOptionsFinder(true);
            $deliveryOptions   = $this->deliveryOptionsFinder->getDeliveryOptions();
            $deliveryOptionKey = current($this->context->cart->getDeliveryOption(null, false, false));
            if (isset($deliveryOptions[$deliveryOptionKey])) {
                $selectedDeliveryOption = $deliveryOptions[$deliveryOptionKey];
                unset($selectedDeliveryOption['product_list']);
            } else {
                $selectedDeliveryOption = 0;
            }
            $payment_options               = $paymentOptionsFinder->present();
            $data['payment_options']       = $payment_options;
            $data['conditions_to_approve'] = $conditionsToApproveFinder->getConditionsToApproveForTemplate();
            if ((!$this->step_4_display) && $last2StepsAsInfoVisibility == 1) {
                $display_as_info = true;
            }
        } else {
            $template        = 'complete_previous_step';
            $data['step_id'] = 3;
        }
        $data['display_as_info'] = $display_as_info;
        return array(
            'data' => $data,
            'template' => $template
        );
    }
    public function displayAjaxLoadPaymentMethods()
    {
        $last3StepsVisibility       = $this->getlLast3StepsVisibility();
        $last2StepsAsInfoVisibility = $this->getlLast2StepsAsInfoVisibility();
        if ($this->step_4_display || ((!$this->step_4_display) && $last3StepsVisibility == 0)) {
            $section_content = $this->stepPaymentMethods_getContent();
            $this->context->smarty->assign($this->prepareTempalteVars($section_content['data']));
            $content = $this->context->smarty->fetch("module:singlepagecheckoutiw/views/templates/front/layouts/{$this->layoutName}/{$section_content['template']}.tpl");
        } else {
            $content = '';
        }
        $response_data = array(
            'content' => $content
        );
        if ((!$this->step_4_display) && $last2StepsAsInfoVisibility == 1) {
            $response_data['display_as_info'] = true;
        } else {
            $response_data['display_as_info'] = false;
        }
        $this->ajaxResponse(array(
            'status' => $this->ajaxResponseStatus,
            'data' => array(
                'PaymentMethods' => $response_data
            ),
            'reload' => $this->reloadSubsequentSections
        ));
    }
    public function displayAjaxValidateCheckout()
    {
        $this->ajaxResponse(array(
            'status' => $this->isCheckoutCompleted() ? 'ok' : 'err'
        ));
    }
    private function cartSummary_getContent()
    {
        $template = 'cart_summary';
        $data     = array();
        if (count($this->cartsummary_errors) > 0) {
            $data['cartsummary_errors'] = $this->cartsummary_errors;
        }
        $cart = $this->cart_presenter->present($this->context->cart);
        if ($cart['vouchers']['allowed'] == 1 && count($cart['vouchers']['added']) > 0) {
            $this->custom_template_vars['page_token'] = Tools::getToken(false);
        }
        $data['cart'] = $cart;
        return array(
            'data' => $data,
            'template' => $template
        );
    }
    public function displayAjaxLoadCartSummary()
    {
        $section_content = $this->cartSummary_getContent();
        $this->context->smarty->assign($this->prepareTempalteVars($section_content['data']));
        $content = $this->context->smarty->fetch("module:singlepagecheckoutiw/views/templates/front/layouts/{$this->layoutName}/{$section_content['template']}.tpl");
        $this->ajaxResponse(array(
            'status' => $this->ajaxResponseStatus,
            'data' => array(
                'CartSummary' => array(
                    'content' => $content
                )
            )
        ));
    }
    public function displayAjaxAddDiscount()
    {
        if ($this->context->cookie->exists() && !($this->isUserLogged && !$this->isTokenValid())) {
            if (CartRule::isFeatureActive() && Tools::getIsset('token') && Tools::getIsset('discountname')) {
                $code = Tools::getValue('discountname');
                $code = trim(strval($code));
                if (empty($code)) {
                    $this->cartsummary_errors[] = $this->trans('You must enter a voucher code.', array(), 'Shop.Notifications.Error');
                } else if (!Validate::isCleanHtml($code)) {
                    $this->cartsummary_errors[] = $this->trans('The voucher code is invalid.', array(), 'Shop.Notifications.Error');
                } else {
                    if (($cartRule = new CartRule(CartRule::getIdByCode($code))) && Validate::isLoadedObject($cartRule)) {
                        $error = $cartRule->checkValidity($this->context, false, true);
                        if (empty($error)) {
                            $this->cartsummary_errors[] = $error;
                        } else {
                            $this->context->cart->addCartRule($cartRule->id);
                        }
                    } else {
                        $this->cartsummary_errors[] = $this->trans('This voucher does not exist.', array(), 'Shop.Notifications.Error');
                    }
                }
            }
        }
        if (!empty($this->cartsummary_errors)) {
            $this->custom_template_vars['cart_summary_promo_code_discountname'] = $code;
        }
        $this->displayAjaxLoadCartSummary();
    }
    public function displayAjaxRemoveVoucher()
    {
        if (Tools::getIsset('remove_voucher')) {
            $cart_rule_id = (int) Tools::getValue('remove_voucher');
            if (Validate::isUnsignedId($cart_rule_id)) {
                $result = $this->context->cart->removeCartRule($cart_rule_id);
                CartRule::autoAddToCart($this->context);
            }
        }
        $this->displayAjaxLoadCartSummary();
    }
    public function displayAjaxGetProgressBarStatus()
    {
        $this->ajaxResponse(array(
            'status' => $this->ajaxResponseStatus,
            'progress_bar' => $this->initPercCompleted()
        ));
    }
}