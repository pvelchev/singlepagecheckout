<?php
 /**
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
 */
 if ( ! defined('_PS_VERSION_')) { exit; } function upgrade_module_1_3_1() { Configuration::updateValue('spciw_select_page_layout', 1); Configuration::updateValue('spciw_select_toggle_last_3_chckout_steps', 0); Configuration::updateValue('spciw_display_the_content_of_last_2_steps_as_info', 1); Configuration::updateValue('spciw_toggle_reassurance', 1); Configuration::updateValue('spciw_toggle_progress_bar', 1); Configuration::updateValue('spciw_display_optional_form_field_company', 1); Configuration::updateValue('spciw_display_optional_form_field_vat_number', 1); Configuration::updateValue('spciw_display_optional_form_field_address_complement', 1); Configuration::updateValue('spciw_display_optional_form_field_phone', 1); Configuration::updateValue('spciw_display_optional_form_field_birthday', 1); Configuration::updateValue('spciw_nl_enabled', 0); DB::getInstance()->executeS('
		CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . '`spciw_nl_condition` (
			`id_condition` int(11) NOT NULL AUTO_INCREMENT ,
			`use_amount` tinyint(1) NOT NULL,
			`amount` decimal(10,2) NOT NULL,
			`to_amount` DECIMAL(10,2) NOT NULL,
			`id_currency` int(11) NOT NULL,
			`convert` tinyint(1) NOT NULL,
			`tax_incl` tinyint(1) NOT NULL,
			`shipping_incl` tinyint(1) NOT NULL,
			`use_count` tinyint(1) NOT NULL,
			`count` int(11) NOT NULL,
			`to_count` INT(11) NOT NULL,
			`apply` varchar(64) NOT NULL,
			`free_shipping` tinyint(1) NOT NULL,
			`id_product_gift` int(11) NOT NULL,
			`id_cart_rule` INT(11) NOT NULL,
			`delay` INT(11) NOT NULL,
			`show_condtitions` tinyint(1) NOT NULL,
			`active` tinyint(1) NOT NULL,
			`date_add` datetime NOT NULL,
			`date_upd` datetime NOT NULL,
			PRIMARY KEY (`id_condition`)
		) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8
	'); DB::getInstance()->executeS('
		CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . '`spciw_nl_condition_gift` (
			`id_condition` int(11) NOT NULL,
			`id_product_gift` int(11) NOT NULL,
			`id_product_attribute` int(11) NOT NULL,
			PRIMARY KEY (`id_condition`,`id_product_gift`)
		) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8
	'); DB::getInstance()->executeS('
		CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . '`spciw_nl_condition_lang` (
			`id_condition` int(11) NOT NULL,
			`id_lang` int(11) NOT NULL,
			`name` varchar(128) NOT NULL,
			PRIMARY KEY (`id_condition`,`id_lang`)
		) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8
	'); DB::getInstance()->executeS('
		CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . '`spciw_nl_condition_rule` (
			`id_cart_rule` int(11) NOT NULL,
			`id_cart` int(11) NOT NULL,
			`id_condition` int(11) NOT NULL
		) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8
	'); DB::getInstance()->executeS('
		CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . '`spciw_nl_issued_vouchers` (
			`id_order` int(11) NOT NULL,
			`id_cart_rule` int(11) NOT NULL,
			`delay` TINYINT(1) NOT NULL,
			`id_condition` INT(11) NOT NULL,
			`delay_date` DATETIME NOT NULL,
			`sent` TINYINT(1) NOT NULL,
			PRIMARY KEY( `id_order`, `id_cart_rule`)
		) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8
	'); return true; } 