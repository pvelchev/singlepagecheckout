<?php

class FrontController extends FrontControllerCore
{
    /**
     * Called before compiling common page sections (header, footer, columns).
     * Good place to modify smarty variables.
     *
     * @see FrontController::initContent()
     */
    public function process()
    {
        parent::process();

        $this->context->smarty->assign([
            'cart' => $this->cart_presenter->present($this->context->cart, true)
        ]);
    }
}
