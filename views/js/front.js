/**
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
*/
(function() {

	if ( $("#checkout").length == 0 ) { return; }

	var scope = '#spciw ';

	var page_containers = {
		'PersonalInformation' : scope + '[id=step_PersonalInformation] .main_content',
		'CustomerAddresses'   : scope + '[id=step_CustomerAddresses] .main_content',
		'ShippingMethods'     : scope + '[id=step_ShippingMethods] .main_content',
		'PaymentMethods'      : scope + '[id=step_PaymentMethods] .main_content',
		'CartSummary'         : scope + '#CartSummary_wrapper'
	};

	var ajax_url = jQuery("#spciw_redirect_url").data("value");
	var ajax_params = {
		type     : "POST",
		data     : {},
		dataType : "json",
		cache    : false
	};

	var progress_bar;

	var current_form_data;

	var settings = {};

	// ===========================================================================
	// GENERAL FUNCTIONS
	// ===========================================================================

	function reload_sections(section_to_reload)
	{
		var reload_next = false;

		for (var i in settings.steps) {
			if (reload_next) {
				load_step_content({}, 'Load' + settings.steps[i].id_name);
				continue;
			}
			if (settings.steps[i].id_name == section_to_reload) {
				reload_next = true;
			}
		}
	}

	function manage_additional_content_settings(resp)
	{
		un_hide_step_content(resp);
		toggle_last_2_steps_as_info(resp);
	}

	function toggle_last_2_steps_as_info(resp)
	{
		if (
			typeof(settings.display_as_info) === "undefined" || settings.display_as_info == 0
		) {
			return;
		}

		var obj;

		for (var stepName in resp.data) {
			if (typeof(resp.data[stepName].display_as_info) === "boolean") {
				obj = $("#step_" + stepName + " .main_content");
				if (resp.data[stepName].display_as_info) {
					obj.addClass("grey-out");
				}
				else {
					obj.removeClass("grey-out");
				}
			}
		}
	}

	function un_hide_step_content(resp)
	{
		if (typeof(settings.hide_last_3_steps) === "undefined" || settings.hide_last_3_steps == 0) {
			return;
		}

		for (var stepName in resp.data) {

			// this will happen when a section name is required with no content
			if (typeof(resp.data[stepName].content) === "undefined") {
				continue;
			}

			// unhide
			if (resp.data[stepName].content != "") {
				$("#step_" + stepName).removeClass("hidden");
			}
			// hide
			else {
				$("#step_" + stepName).addClass("hidden");
			}
		}
	}

	function restore_original_address_form_data(fields_container)
	{
		$(fields_container).find("input[name=CustomerAddresses_id_customer]").val(current_form_data.id_customer);
		$(fields_container).find("input[name=CustomerAddresses_id_address]").val(current_form_data.id_address);
		$(fields_container).find("input[name=CustomerAddresses_firstname]").val(current_form_data.firstname);
		$(fields_container).find("input[name=CustomerAddresses_lastname]").val(current_form_data.lastname);
		$(fields_container).find("input[name=CustomerAddresses_company]").val(current_form_data.company);
		$(fields_container).find("input[name=CustomerAddresses_vat_number]").val(current_form_data.vat_number);
		$(fields_container).find("input[name=CustomerAddresses_address]").val(current_form_data.address);
		$(fields_container).find("input[name=CustomerAddresses_address_complement]").val(current_form_data.address_complement);
		$(fields_container).find("input[name=CustomerAddresses_zip_postal_code]").val(current_form_data.zip_postal_code);
		$(fields_container).find("#address_country").val(current_form_data.country);
		$(fields_container).find("input[name=CustomerAddresses_city]").val(current_form_data.city);
		$(fields_container).find("input[name=CustomerAddresses_phone]").val(current_form_data.phone);

		$(fields_container).find("input[name=CustomerAddresses_is_delivery_address]").val(current_form_data.is_delivery_address);

		current_form_data = null;
	}

	function storeCurrentAddressFormData(fields_container)
	{
		current_form_data = {};

		current_form_data.id_customer = $(fields_container).find("input[name=CustomerAddresses_id_customer]").val();
		current_form_data.id_address = $(fields_container).find("input[name=CustomerAddresses_id_address]").val();
		current_form_data.firstname = $(fields_container).find("input[name=CustomerAddresses_firstname]").val();
		current_form_data.lastname = $(fields_container).find("input[name=CustomerAddresses_lastname]").val();
		current_form_data.company = $(fields_container).find("input[name=CustomerAddresses_company]").val();
		current_form_data.vat_number = $(fields_container).find("input[name=CustomerAddresses_vat_number]").val();
		current_form_data.address = $(fields_container).find("input[name=CustomerAddresses_address]").val();
		current_form_data.address_complement = $(fields_container).find("input[name=CustomerAddresses_address_complement]").val();
		current_form_data.zip_postal_code = $(fields_container).find("input[name=CustomerAddresses_zip_postal_code]").val();
		current_form_data.country = $(fields_container + " #address_country").val();
		current_form_data.city = $(fields_container).find("input[name=CustomerAddresses_city]").val();
		current_form_data.phone = $(fields_container).find("input[name=CustomerAddresses_phone]").val();

		current_form_data.is_delivery_address = $(fields_container).find("input[name=CustomerAddresses_is_delivery_address]").val();
	}

	function ajax_call(data, ajaxAction, callback)
	{
		add_ajax_required_params(data, ajaxAction);
		ajax_params.data = data;
		ajax_call_execute(ajax_params, callback);
	}

	function ajax_call_execute(params, callback)
	{
		// console.dir(params);

		$.ajax(ajax_url, params).done( function(resp) {
			// console.dir(resp);

			var section_obj;
			var section_to_reload = false;

			if (typeof(resp.status) === "string" && (resp.status == "ok" || resp.status == "err")) {

				// redirect instead of process?
				if (typeof(resp.redirect) === "string") {
					document.location.href = resp.redirect;
					return;
				}

				if (typeof(resp.data) === "object") {
					for (var s in resp.data) {

						// we need an existing container
						if (typeof(page_containers[s]) !== "undefined") {
							section_to_reload = s;

							// add the content into that container
							if (typeof(resp.data[s].content) === "string") {
								jQuery(page_containers[s]).html(resp.data[s].content);
							}
						}
					}
				}

				manage_additional_content_settings(resp);

				if (typeof(resp.token) === "string") {
					$(scope).data("token", resp.token);
				}

				if (typeof(callback) === "function") {
					callback(resp);
				}

				if (typeof(resp.reload) === "boolean" && resp.reload && section_to_reload !== false) {
					reload_sections(section_to_reload);
				}
			}

		}).fail(ajax_fail_handler);
	}

	function add_ajax_required_params(data, ajaxAction)
	{
		data['ajax'] = 1;
		data['action'] = ajaxAction;
	}

	function ajax_fail_handler(jqXHR, textStatus, errorThrown)
	{
		// TODO: UX?
		console.error(textStatus);
		console.error(jqXHR);
		console.error(errorThrown);
	}

	function load_step_content(data, ajaxAction, callback)
	{
		ajax_call(data, ajaxAction, callback);
	}

	// ===========================================================================
	// INIT (this will load the base page template)
	// ===========================================================================

	(function() {

		let data = {};

		add_ajax_required_params(data, 'LoadBaseLayout');

		ajax_params.data = data;

		$.ajax(ajax_url, ajax_params).done( function(resp) {
			// console.dir(resp);

			settings = $.extend(settings, resp.data.settings);
			if (settings.is_virtual) {
				// if the cart is virtual, remove the shipping container to avoid subsequent content loading
				for (var i in page_containers) {
					if (i == 'ShippingMethods') {
						delete page_containers[i];
					}
				}
			}

			$("#content").html(resp.data.content);

			if (settings.display_progress_bar == 1) {
				progress_bar = $("#spciw_progress_bar");
			}

			for (var i in resp.data.steps) {
				if (typeof(resp.data.steps[i].id_name) === "string") {
					load_step_content({}, 'Load' + resp.data.steps[i].id_name);
				}
			}

			$("#content").css("display", "block");

		}).fail(ajax_fail_handler);
	})();

	// ===========================================================================
	// EVENT HANDLERS
	// ===========================================================================

	// *** Disable event propagation
	$(document).on({
		click: function(e) {
			e.stopPropagation();
		}
	}, scope + ".step_wrapper .overlay");

	// *** tab content switcher
	$(document).on({
		click: function(e) {
			e.preventDefault();

			var link = $(this).attr("href");

			$(this).closest(".step_wrapper").find(".tabcontent[id]").addClass("hidden");

			$(scope + link).removeClass("hidden");
		}
	}, scope + "a.tablink");

	// *** show/hide password fields content
	$(document).on({
		click: function(e) {
			e.preventDefault();

			var obj;

			obj = $(this).closest(".field_wrapper").find("input[type=text]");

			if (obj.length > 0) {
				obj.attr("type", "password");
				$(this).html( $(this).data("show") );
			}
			else {
				obj = $(this).closest(".field_wrapper").find("input[type=password]");

				if (obj.length > 0) {
					obj.attr("type", "text");
					$(this).html( $(this).data("hide") );
				}
			}
		}
	}, scope + ".show_hide_pass[data-show][data-hide]");

	// *** radio boxes
	$(document).on({
		click: function() {

			$(this).closest(".field_wrapper").find(".radio_icon .icon_body").each( function() {
				$(this).removeClass("active");
			});

			$(this).find(".icon_body").addClass("active");
		}
	}, scope + ".field_wrapper .user_selection.radio");

	// *** check boxes
	$(document).on({
		click: function() {

			var obj = $(this).find(".icon_body").hasClass("active");

			if (obj) {
				$(this).find(".icon_body").removeClass("active");
			}
			else {
				$(this).find(".icon_body").addClass("active");
			}
		}
	}, scope + ".field_wrapper .user_selection.check");

	// ===========================================================================
	// PERSONAL INFORMATION
	// ===========================================================================

	// *** Sign in
	var signin_form = (function() {

		var fields_container = scope + "#step_PersonalInformation #checkout_login_form";

		// click "Continue"
		$(document).on({
			click: function () {

				var email = $(fields_container).find("[name=PersonalInformation_signin_email]").val();
				var password = $(fields_container).find("[name=PersonalInformation_signin_password]").val();

				var data = {
					email : email,
					password : password
				};

				ajax_call(data, 'UserSignIn', function() {
					refresh_cart_summary_content();
					progressbar_refresh();
				});
			}
		}, fields_container + " .signmein");

		return true;
	})();

	// *** Order as guest
	var orderasguest_form = (function() {

		var fields_container = scope + "#step_PersonalInformation #checkout_order_as_guest_form";

		// click "Continue"
		$(document).on({
			click: function () {

				var data = {};

				data.id_customer = ''; // ?

				data.id_gender = $(fields_container).find(".radio_icon .icon_body.active").data("value");
				/*
				if (typeof(data.id_gender) !== "number" || (data.id_gender != 1 && data.id_gender != 2)) {
					data.id_gender = 1;
				}
				*/

				data.firstname = $(fields_container).find("input[name=PersonalInformation_guest_firstname]").val();
				data.lastname = $(fields_container).find("input[name=PersonalInformation_guest_lastname]").val();
				data.email = $(fields_container).find("input[name=PersonalInformation_guest_email]").val();
				data.password = $(fields_container).find("input[name=PersonalInformation_guest_password]").val();

				// optional
				var birthday = $(fields_container).find("input[name=PersonalInformation_guest_birthday]");
				if (birthday.length > 0) {
					data.birthday = birthday.val();
				}
				else {
					data.birthday = "";
				}

				data.optin = $(fields_container).find(".check_icon .icon_body.active[data-name=optin]").length == 0 ? 0 : 1;
				data.newsletter = $(fields_container).find(".check_icon .icon_body.active[data-name=newsletter]").length == 0 ? 0 : 1;

				ajax_call(data, 'UserOrderAsGuest', function() {
					refresh_cart_summary_content();
					progressbar_refresh();
				});
			}
		}, fields_container + " .orderasguest");

		return true;
	})();

	// ===========================================================================
	// CUSTOMER ADDRESSES
	// ===========================================================================

	// *** Set delivery or invoice address id
	$(document).on({
		click: function () {

			var container = $(this).closest("[id][data-address-type]").find(".address_item");
			container.removeClass("active");
			container.find(".icon_body").removeClass("active");
			$(this).addClass("active");
			$(this).find(".icon_body").addClass("active");

			var address_type = $(this).closest("[id][data-address-type]").data("address-type");

			var data = {
				token : $(scope).data("token"),
				address_id : $(this).data("address-id"),
				address_type : address_type
			};

			// refresh only if the address is type delivery
			var callback = function() {};
			if (address_type == "delivery") {
				callback = function () {
					refresh_cart_summary_content();
					progressbar_refresh();
				};
			}

			ajax_call(data, 'SetAddressId', callback);
		}
	}, scope + ".address_item");

	// *** Same as billing
	$(document).on({
		click: function () {

			ajax_call({}, 'ShippingAddressSameAsBilling');
		}
	}, scope + ".shippingaddresssameasbilling");

	// *** Different invoice address
	$(document).on({
		click: function () {

			ajax_call({}, 'DifferentInvoiceAddress');
		}
	}, scope + ".differentinvoiceaddress");

	// *** Add new address
	$(document).on({
		click: function () {

			ajax_call({}, 'AddNewAddress');
		}
	}, scope + ".addnewaddress");

	// *** Edit address Cancel
	$(document).on({
		click: function (e) {
			e.preventDefault();

			ajax_call({}, 'LoadCustomerAddresses');
		}
	}, scope + ".editcancel");

	// *** Edit/Delete address action
	$(document).on({
		click: function (e) {
			e.stopPropagation();

			var token = $(this).closest("[data-token]").data("token");
			var address_id = $(this).closest("[data-address-id]").data("address-id");
			var action = $(this).data("link-action");

			var data = {
				token : token,
				address_id : address_id
			};

			if (action == 'EditAddress') {
				ajax_call(data, action);
			}
			else if (action == 'DeleteAddress') {
				ajax_call(data, action, function() {
					progressbar_refresh();
				});
			}
		}
	}, scope + ".addressaction");

	// *** Change country
	var changecountry_form = (function() {

		var fields_container = scope + "#step_CustomerAddresses";

		// click "Continue"
		$(document).on({
			change: function () {

				storeCurrentAddressFormData(fields_container);

				var data = {};

				data.id_country = $(this).val();

				ajax_call(data, 'ChangeCountry', function() {
					restore_original_address_form_data(fields_container);
				});
			}
		}, fields_container + " #address_country");

		return true;
	})();

	// *** Save address
	var saveaddress_form = (function() {

		var fields_container = scope + "#step_CustomerAddresses";

		// click "Save"
		$(document).on({
			click: function () {

				storeCurrentAddressFormData(fields_container);

				var data = {};

				data.token = $(scope).data("token");
				data.id_customer = $(fields_container).find("input[name=CustomerAddresses_id_customer]").val();
				data.id_address = $(fields_container).find("input[name=CustomerAddresses_id_address]").val();
				data.firstname = $(fields_container).find("input[name=CustomerAddresses_firstname]").val();
				data.lastname = $(fields_container).find("input[name=CustomerAddresses_lastname]").val();
				data.company = $(fields_container).find("input[name=CustomerAddresses_company]").val();
				data.vat_number = $(fields_container).find("input[name=CustomerAddresses_vat_number]").val();
				data.address1 = $(fields_container).find("input[name=CustomerAddresses_address]").val();
				data.address2 = $(fields_container).find("input[name=CustomerAddresses_address_complement]").val();
				data.postcode = $(fields_container).find("input[name=CustomerAddresses_zip_postal_code]").val();
				data.city = $(fields_container).find("input[name=CustomerAddresses_city]").val();
				data.phone = $(fields_container).find("input[name=CustomerAddresses_phone]").val();
				var id_state = $(fields_container + " #address_state").val();
				if (typeof(id_state) === "string") { data.id_state = id_state; }
				data.id_country = $(fields_container + " #address_country").val();
				/*
				data.use_same_address = true;
				if ( $(fields_container + " #use_same_address:not(.active)").length == 1) {
					// the user has turned the checkbox off
					data.use_same_address = false;
				}
				*/
				data.is_delivery_address = $(fields_container).find("input[name=CustomerAddresses_is_delivery_address]").val();

				ajax_call(data, 'SaveAddress', function() {
					restore_original_address_form_data(fields_container);
					refresh_cart_summary_content();
					progressbar_refresh();
				});
			}
		}, fields_container + " .saveaddress");

		return true;
	})();

	// ===========================================================================
	// SHIPPING METHODS
	// ===========================================================================

	function force_update_shipping_method()
	{
		var obj = $(scope + "#step_ShippingMethods .shipping_option .icon_body.active");
		if (obj.length == 0) { // by default one shipping method is selected, so this shouldn't happend but let's play safe
			return;
		}

		obj.closest(".shipping_option").click();
	}

	// *** Update shipping method
	$(document).on({
		click: function(e) {
			e.stopPropagation();

			var delivery_address_id = $(this).closest("[data-delivery-address-id]").data("delivery-address-id");
			var carrier_id = $(this).data("carrier-id");

			var data = {
				delivery_address_id : delivery_address_id,
				carrier_id : carrier_id
			};

			// recyclable
			if ($(scope + "#recycled_packaging_selected").length > 0) {
				data['recyclable'] = $(scope + "#recycled_packaging_selected.active").length == 1 ? 1 : 0;
			}

			// gift
			data['gift'] = 0;
			data['gift_message'] = "";
			if ($(scope + "#gift_selected").length > 0) { // allowed?
				var gift_selected = $(scope + "#gift_selected.active").length > 0 ? true : false;
				if (gift_selected) {
					data['gift'] = 1;
					data['gift_message'] = $(scope + "#gift_message").val();
				}
			}

			// delivery message
			if ($(scope + "#delivery_message").length > 0) {
				var delivery_message = $(scope + "#delivery_message").val();
				if (delivery_message.length > 0) {
					data['delivery_message'] = delivery_message;
				}
			}

			$(this).closest("[data-delivery-address-id]").find(".shipping_option").removeClass("active");
			$(this).closest("[data-delivery-address-id]").find(".shipping_option .radio_icon .icon_body").removeClass("active");
			$(this).addClass("active");
			$(this).find(".radio_icon .icon_body").addClass("active");

			ajax_call(data, 'UpdateShippingMethod', function() {
				refresh_cart_summary_content();
			});
		}
	}, scope + "#step_ShippingMethods .shipping_option");

	// gift selected
	$(document).on({
		click: function() {

			if ($(this).find("#gift_selected.active").length > 0) {
				$(scope + "#gift_message_wrapper").removeClass("hidden");
			}
			else {
				$(scope + "#gift_message_wrapper").addClass("hidden");
			}

			force_update_shipping_method();
		}
	}, scope + "#gift_selected_wrapper");

	// recycled package de/selected
	$(document).on({
		click: function() {
			force_update_shipping_method();
		}
	}, scope + "#recycled_packaging_wrapper");

	// ===========================================================================
	// PAYMENT METHODS
	// ===========================================================================

	function toggle_submit_order_button(state)
	{
		if (state) {
			$(scope + "#payment_methods_submit_button").prop("disabled", false);
		}
		else {
			$(scope + "#payment_methods_submit_button").prop("disabled", true);
		}
	}

	function validate_payment_state()
	{
		var payment_method_selected = $(scope + "#step_PaymentMethods .payment_methods_wrapper .payment_option .icon_body.active").length > 0 ? true : false;

		var conditions_to_approve = true;
		$(scope + "#payment_methods_conditions_to_approve [data-condition-name] .icon_body").each( function() {
			if ( ! $(this).hasClass("active")) {
				conditions_to_approve &= false;
			}
		});

		if (payment_method_selected && conditions_to_approve) {
			toggle_submit_order_button(true);
			update_progress_bar_status(true);
		}
		else {
			toggle_submit_order_button(false);
			update_progress_bar_status(false);
		}
	}

	// *** Conditions to approve
	$(document).on({
		click: function() {
			validate_payment_state();
		}
	}, scope + ".field_wrapper .user_selection.check[data-condition-name]");

	// *** Select payment method
	$(document).on({
		click: function(e) {
			e.stopPropagation();

			$(this).closest(".payment_methods_wrapper").find(".payment_option .icon_body").removeClass("active");
			$(this).closest(".payment_methods_wrapper").find(".payment_option .payment_name .payment_name_wrapper").removeClass("bold");
			$(this).closest(".payment_methods_wrapper").find(".additional_info_wrapper").addClass("hidden");

			$(this).closest(".payment_option").find(".icon_body").addClass("active");
			$(this).closest(".payment_option").find(".payment_name_wrapper").addClass("bold");
			$(this).closest(".payment_option").find(".additional_info_wrapper").removeClass("hidden");

			validate_payment_state();
		}
	}, scope + "#step_PaymentMethods .payment_option .select_icon" + ", " + scope + "#step_PaymentMethods .payment_option .payment_name_wrapper");

	// *** Submit order button
	$(document).on({
		click: function() {

			var payment_selected_url = $(scope + "#step_PaymentMethods .payment_option").data("action");

			if (typeof(payment_selected_url) !== "string") {
				console.error("Invalid payment method selected");
				return;
			}

			toggle_submit_order_button(false);
			validate_checkout(payment_selected_url);
		}
	}, scope + "#payment_methods_submit_button");

	// validate checkout
	function validate_checkout(payment_selected_url)
	{
		var params = {};

		add_ajax_required_params(params, 'validatecheckout');

		ajax_params.data = params;

		$.ajax(ajax_url, ajax_params).done( function(resp) {
			// console.dir(resp);

			if (typeof(resp.status) === "string") {
				if (resp.status == 'ok') {
					document.location.href = payment_selected_url;
				}
				else if (resp.status == 'err') {
					document.location.reload(true);
				}
			}
		}).fail(ajax_fail_handler);
	};

	// ===========================================================================
	// CART SUMMARY
	// ===========================================================================

	function refresh_cart_summary_content()
	{
		ajax_call({}, 'LoadCartSummary');
	}

	// *** Add promo code
	$(document).on({
		click: function() {

			var data = {
				token : $(scope + "#cart_summary_promo_code_token").val(),
				discountname : $(scope + "#cart_summary_promo_code_discountname").val()
			};

			ajax_call(data, 'AddDiscount');
		}
	}, scope + "#cart_summary_promo_code_add");

	// *** Remove voucher
	$(document).on({
		click: function(e) {
			e.preventDefault();

			var data = {
				remove_voucher : $(this).data("remove-voucher")
			};

			ajax_call(data, 'RemoveVoucher');
		}
	}, scope + "#cart_summary_voucher_list .removevoucher");

	// ===========================================================================
	// PROGRESS BAR
	// ===========================================================================

	function progressbar_refresh()
	{
		if (settings.display_progress_bar == 0) {
			return;
		}

		var data = {};

		add_ajax_required_params(data, 'GetProgressBarStatus');

		ajax_params.data = data;

		$.ajax(ajax_url, ajax_params).done( function(resp) {
			// console.dir(resp);

			progress_bar.data("steps-status", resp.progress_bar.steps_status);
			var wrapper = progress_bar.find(".progress-bar[aria-valuenow]");
			wrapper.attr("aria-valuenow", resp.progress_bar.perc_completed);
			wrapper.css("width", resp.progress_bar.perc_completed + "%");
			wrapper.html(resp.progress_bar.perc_completed + "%");

		}).fail(ajax_fail_handler);
	}

	function update_progress_bar_status(completed)
	{
		if (settings.display_progress_bar == 0) {
			return;
		}

		var perc_completed;
		var steps_status = progress_bar.data("steps-status");
		var steps = steps_status.split(',');
		var steps_length = steps.length;

		if (completed) {
			steps[steps_length-1] = 1;
			perc_completed = '100';
		}
		else {
			steps[steps_length-1] = 0;
			var onep = 100 / steps_length;
			var total_steps_completed = 1;
			for (var i in steps) {
				if (steps[i] == 1) {
					total_steps_completed++;
				}
			}
			perc_completed = Math.ceil((onep * total_steps_completed) - onep);
		}

		steps = steps.join(',');
		progress_bar.data("steps-status", steps);

		var wrapper = progress_bar.find(".progress-bar[aria-valuenow]");
		wrapper.attr("aria-valuenow", perc_completed);
		wrapper.css("width", perc_completed + "%");
		wrapper.html(perc_completed + "%");
	}

})();
