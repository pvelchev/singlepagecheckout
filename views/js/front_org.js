/**
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
*/
var spciw_get_request_url = (function() {
	return jQuery("#spciw_redirect_url").data("value");
})();

if (jQuery("#cart #main .cart-summary .checkout.cart-detailed-actions a").length > 0 && jQuery("#spciw_redirect_url").length > 0) {
	var url = spciw_get_request_url;
	var obj = jQuery("#cart #main .cart-summary .checkout.cart-detailed-actions a");
	obj.attr("href", url);
	// obj.addClass("is-not-hidden");
}
else if (jQuery("#checkout #wrapper .container #content").length > 0) {
	document.location.href = spciw_get_request_url;
	throw "a redirect is happening"
}

(function(jQuery) {

	var scope = "body#spciw";

	var ajax = {
		url : spciw_get_request_url
	};

	var html_containers = {
		'personal_information' : scope + ' .spc_content_wrapper [id=step_personal_information] .content',
		'addresses'            : scope + ' .spc_content_wrapper [id=step_addresses] .content',
		'shipping_method'      : scope + ' .spc_content_wrapper [id=step_shipping_method] .content',
		'payment'              : scope + ' .spc_content_wrapper [id=step_payment] .content',
		'cart_summary'         : scope + ' #spc_cart_summary_wrapper #js-checkout-summary'
	};

	var ajax_params = {
		type     : "POST",
		data     : null,
		dataType : "json",
		cache    : false
	};

	// shall we display the sections that do not have editable content?
	// 0 = don't hide
	var hide_last_3_steps = parseInt(jQuery(scope).data('hidelast3steps'));

	var display_last_2_steps_as_info = parseInt(jQuery(scope).data('displaylast2stepsasinfo'));

	// shall we display the optional form fields
	// 1 = don't hide
	var display_optional_form_fields = jQuery(scope).data('displayoptionalformfields');

	// see "cart_updater"
	// call if have to update the cart summary info
	var update_checkout_summary;

	// hide optional form fields
	function toggle_optional_form_fields() {

		var obj, parts, i;

		for (i in display_optional_form_fields) {

			parts = display_optional_form_fields[i].split(":");

			obj = jQuery(scope + " #step_addresses .jsaddressform-inner .form-fields input[name=" + parts[0] + "].form-control").closest(".form-group");
			if (obj.length > 0) {
				// don't hide
				if (parts[1] == "1") {
					obj.show();
				}
				else {
					obj.hide();
				}
				continue;
			}

			obj = jQuery(scope + " #step_personal_information #customer-form .form-group input[name=" + parts[0] + "].form-control").closest(".form-group");
			if (obj.length > 0) {
				// don't hide
				if (parts[1] == "1") {
					obj.show();
				}
				else {
					obj.hide();
				}
			}
		}
	}
	toggle_optional_form_fields();
	jQuery(scope + " #content.inixframe").removeClass("hidden");
	/*
	function toggle_optional_form_fields() {
		jQuery(scope + " form .jsaddressform-inner input:not([required])").each( function() {
			jQuery(this).closest('.form-group').hide();
		});
	}
	if (display_optional_form_fields == 0) {
		if (jQuery(scope + " #checkout-guest-form [name=optin]").length > 0) {
			jQuery(scope + " #checkout-guest-form [name=optin]").closest(".form-group").hide();
			jQuery(scope + " #checkout-guest-form [name=newsletter]").closest(".form-group").hide();
			jQuery(scope + " #checkout-guest-form [name=birthday]").closest(".form-group").hide();
		}

		toggle_optional_form_fields();

		jQuery(scope + " #content.inixframe").removeClass("hidden");
	}
	*/

	function add_ajax_required_params(data, action_value) {
		data.push({name: "ajax", value: 1});
		data.push({name: "action", value: action_value});
	}

	function ajax_fail_handler(jqXHR, textStatus, errorThrown) {
		// TODO: UX?
		console.error(textStatus);
		console.error(jqXHR);
		console.error(errorThrown);
	}

	// grey-out overlay; prevent clicking on the elements below
	jQuery(scope + " section.checkout-step .content .overlay").on({
		click: function(e) {
			e.stopPropagation();
		}
	});

	// progress bar
	var progress_bar = jQuery(scope + " #progress_bar");
	var get_progress_bar_status = function() {

		if (progress_bar.length == 0) {
			return;
		}

		var data = [];

		add_ajax_required_params(data, 'getProgressBarStatus');

		ajax_params.data = jQuery.param(data);

		jQuery.ajax(ajax.url, ajax_params).done( function(resp) {
			update_progress_bar_data(progress_bar, resp.progress_bar);
		}).fail(ajax_fail_handler);
	};

	var update_progress_bar_data = function(progress_bar, resp) {
		progress_bar.data("steps-status", resp.steps_status);
		var wrapper = progress_bar.find(".progress-bar[aria-valuenow]");
		wrapper.attr("aria-valuenow", resp.perc_completed);
		wrapper.css("width", resp.perc_completed + "%");
		wrapper.html(resp.perc_completed + "%");
	};

	var update_progress_bar_status = function(completed) {

		if (progress_bar.length == 0) {
			return;
		}

		var perc_completed;
		var steps_status = progress_bar.data("steps-status");
		var steps = steps_status.split(',');
		var steps_length = steps.length;

		if (completed) {
			steps[steps_length-1] = 1;
			perc_completed = '100';
		}
		else {
			steps[steps_length-1] = 0;
			var onep = 100 / steps_length;
			var total_steps_completed = 1;
			for (var i in steps) {
				if (steps[i] == 1) {
					total_steps_completed++;
				}
			}
			perc_completed = Math.ceil((onep * total_steps_completed) - onep);
		}

		steps = steps.join(',');
		progress_bar.data("steps-status", steps);

		var wrapper = progress_bar.find(".progress-bar[aria-valuenow]");
		wrapper.attr("aria-valuenow", perc_completed);
		wrapper.css("width", perc_completed + "%");
		wrapper.html(perc_completed + "%");
	};

	// toggle password visibility
	var toggle_password_visibility = function() {

		var button_obj = scope + ' button[data-action="show-password"]';

		var handler = function () {
			var elm = $(this).closest('.input-group').children('input.js-visible-password');
			if (elm.attr('type') === 'password') {
				elm.attr('type', 'text');
				$(this).text($(this).data('textHide'));
			} else {
				elm.attr('type', 'password');
				$(this).text($(this).data('textShow'));
			}
		};

		jQuery(document).off('click', button_obj, handler).on('click', button_obj, handler);
	};

	var submit_request = function (params, callback) {

		jQuery.ajax(ajax.url, params).done( function(resp) {
			// console.dir(resp);

			var section_obj;

			if (typeof(resp.status) === "string" && resp.status == "ok") {

				// redirect?
				if (typeof(resp.redirect) === "string") {
					document.location.href = resp.redirect;
					return;
				}

				for (var s in resp.data) {
					if (typeof(html_containers[s]) !== "undefined") {
						if (s === 'personal_information') {
							toggle_password_visibility();
						}
						else if (s == 'cart_summary') {
							jQuery(html_containers[s]).replaceWith(resp.data[s].content);
							continue;
						}

						if (typeof(resp.data[s].content) === "string") {
							jQuery(html_containers[s]).html(resp.data[s].content);
						}
						else if (typeof(resp.data[s].content) === "boolean" && resp.data[s].content == true) {
							link_click_handler([], s.replace('_','') + 'reload');
						}

						section_obj = jQuery(scope + " .spc_content_wrapper [id$=" + s + "].checkout-step");
						var display_section_received = typeof(resp.data[s].display) === "boolean";
						if (display_section_received) {
							if (resp.data[s].display) {
								section_obj.removeClass("ps-hidden");
								if (display_last_2_steps_as_info == 1) {
									section_obj.find(".content.grey-out").removeClass("grey-out");
									section_obj.find(".content .overlay").remove();
								}
							}
							else {
								if (hide_last_3_steps == 1 && display_last_2_steps_as_info == 0) {
									section_obj.addClass("ps-hidden");
								}
								else if (hide_last_3_steps == 0 && display_last_2_steps_as_info == 1) {
									if (section_obj.find(".content:not(.grey-out)").find(".overlay").length > 0) {
										section_obj.find(".content").addClass("grey-out");
									}
								}
							}
						}
						if (hide_last_3_steps == 0 && display_section_received && resp.data[s].display == false && display_last_2_steps_as_info == 0) {
							section_obj.removeClass("ps-hidden");
							var step_number = parseInt(jQuery(html_containers[s]).closest(".checkout-step[data-step-number]").data("step-number"));
							jQuery(html_containers[s]).html('<p>Please complete step ' + (step_number-1) + '</p>');
						}
					}
				}

				// if (display_optional_form_fields == 0) {
					toggle_optional_form_fields();
				// }

				if (typeof(callback) === "function") {
					callback();
				}
			}
		}).fail(ajax_fail_handler);
	};

	var form_click_handler = function(form_id, ajax_action, callback) {

		var form_data = jQuery(form_id).serializeArray();
		add_ajax_required_params(form_data, ajax_action);

		ajax_params.data = jQuery.param(form_data);

		submit_request(ajax_params, callback);
	};

	var link_click_handler = function(data, ajax_action, callback) {

		add_ajax_required_params(data, ajax_action);

		ajax_params.data = jQuery.param(data);

		submit_request(ajax_params, callback);
	};

	var get_use_same_address_value = function() {

		return jQuery(scope + ' #saveaddress-form .differentinvoiceaddress').length == 0 ? "0" : "1";
	};

	/*
	 * Cart : Update
	 * -------------------------------------------------------
	 *
	 */
	var cart_updater = (function() {

		var trigger_update = function() {
			link_click_handler([], 'updatecartsummary');
		};

		// exit the bubble
		update_checkout_summary = trigger_update;

		return true;
	})();

	/*
	 * Personal Information : Sign in
	 * -------------------------------------------------------
	 *
	 */
	var signin_form = (function() {

		var form_id = scope + " #login-form";

		// click "Continue"
		jQuery(document).on({
			click: function () {
				form_click_handler(form_id, jQuery(this).data("link-action"), function() {
					get_progress_bar_status();
				});
			}
		}, form_id + " .signmein");

		return true;
	})();

	/*
	 * Personal Information : Order as a guest
	 * -------------------------------------------------------
	 *
	 */
	var orderasaguest_form = (function() {

		var form_id = scope + " #customer-form";

		// click "Continue"
		jQuery(document).on({
			click: function () {
				form_click_handler(form_id, jQuery(this).data("link-action"), function() {
					get_progress_bar_status();
				});
			}
		}, form_id + " .order_as_a_guest");

		return true;
	})();

	/*
	 * Addresses
	 * -------------------------------------------------------
	 *
	 */

	var address_selected = (function() {

		var form_id = scope + " .spc_content_wrapper #saveaddress-form";

		jQuery(document).on({
			click: function (e) {
				// e.stopPropagation();
				// e.preventDefault();

				// address container
				var container = jQuery(this).closest("[data-address-id]");

				// already selected, skip
				if (container.hasClass('selected')) {
					return;
				}

				// get the selected address id
				var select_address_id = container.data("address-id");

				// get selected address type
				var selected_address_type = jQuery(this).closest("[data-address-type]").data("address-type");

				// enable/disable radio buttons
				// jQuery(form_id + " input[name=id_address_" + selected_address_type + "]").prop("checked", false);
				// jQuery(form_id + " input[name=id_address_" + selected_address_type + "][value=" + select_address_id + "]").prop("checked", true);

				// make it selected (blue border)
				jQuery(this).closest('.address-item').siblings('.selected').removeClass('selected');
				container.addClass('selected');

				// data
				var action = jQuery(this).closest("[data-link-action]").data("link-action");
				var use_same_address = get_use_same_address_value();
				var data = [
					{name: "address_id", value: select_address_id},
					{name: "address_type", value: selected_address_type},
					{name: "use_same_address", value: use_same_address}
				];

				link_click_handler(data, action, update_checkout_summary);
			}
		}, form_id + " .address-item[data-address-id] .radio-block");

		return true;
	})();

	/*
	 * Addresses : Edit address
	 * -------------------------------------------------------
	 *
	 */
	var delivery_addresses_edit = (function() {

		// var form_id = scope + " #delivery-addresses";
		var form_id = scope + " #saveaddress-form";

		// click "Continue"
		jQuery(document).on({
			click: function (e) {
				e.preventDefault();

				var action = jQuery(this).data("link-action");
				var token = jQuery(this).closest('[data-token]').data('token');
				var address_id = jQuery(this).closest('[data-address-id]').data('address-id');
				var address_type = jQuery(this).closest('[data-address-type]').data('address-type');
				var use_same_address = get_use_same_address_value();

				var data = [
					{name: "token", value: token},
					{name: "address_id", value: address_id},
					{name: "address_type", value: address_type},
					{name: "use_same_address", value: use_same_address}
				];

				link_click_handler(data, action);
			}
		}, form_id + " .editaddress");

		return true;
	})();

	/*
	 * Addresses : Save address
	 * -------------------------------------------------------
	 *
	 */
	var saveaddress_form = (function() {

		var form_id = scope + " #saveaddress-form";

		// click "Save"/"Continue"
		jQuery(document).on({
			click: function () {
				form_click_handler(form_id, jQuery(this).data("link-action"), function() {
					get_progress_bar_status();
				});
			}
		}, form_id + " .saveaddress[data-link-action]");

		return true;
	})();

	/*
	 * Addresses : Cancel (Save) address
	 * -------------------------------------------------------
	 *
	 */
	var canceleditaddress_link = (function() {

		var form_id = scope + " #saveaddress-form";

		jQuery(document).on({
			click: function () {

				var action = jQuery(this).data("link-action");
				var address_type = jQuery(this).closest('[data-address-type]').data('address-type');

				var data = [
					{name: "address_type", value: address_type}
				];

				link_click_handler(data, action);
			}
		}, form_id + " .canceleditaddress[data-link-action]");

		return true;
	})();

	/*
	 * Addresses : Delete address
	 * -------------------------------------------------------
	 *
	 */
	var deleteaddress_link = (function() {

		var form_id = scope + " #saveaddress-form";

		jQuery(document).on({
			click: function () {

				var action = jQuery(this).data("link-action");
				var token = jQuery(this).closest('[data-token]').data('token');
				var address_id = jQuery(this).closest('[data-address-id]').data('address-id');
				var use_same_address = get_use_same_address_value();

				var data = [
					{name: "token", value: token},
					{name: "address_id", value: address_id},
					{name: "use_same_address", value: use_same_address}
				];

				link_click_handler(data, action, function() {
					get_progress_bar_status();
				});
			}
		}, form_id + " .deleteaddress[data-link-action]");

		return true;
	})();

	/*
	 * Addresses : Add new
	 * -------------------------------------------------------
	 *
	 */
	var addnewaddress_link = (function() {

		var form_id = scope + " #saveaddress-form";

		jQuery(document).on({
			click: function () {

				var action = jQuery(this).data("link-action");
				var address_type = jQuery(this).closest('[data-address-type]').data('address-type');

				var data = [
					{name: "address_type", value: address_type}
				];

				link_click_handler(data, action);
			}
		}, form_id + " .addnewaddress[data-link-action]");

		return true;
	})();

	/*
	 * Addresses : Different invoice and shipping addresses
	 * -------------------------------------------------------
	 *
	 */
	var addnewdeliveryaddress_link = (function() {

		var form_id = scope + " #saveaddress-form";

		jQuery(document).on({
			click: function () {

				var action = jQuery(this).data("link-action");

				link_click_handler([], action);
			}
		}, form_id + " .differentinvoiceaddress[data-link-action]");
	})();

	/*
	 * Addresses : Shipping address same as billing
	 * -------------------------------------------------------
	 *
	 */
	var shippingaddresssameasbilling_link = (function() {

		var form_id = scope + " #saveaddress-form";

		jQuery(document).on({
			click: function () {

				var action = jQuery(this).data("link-action");

				link_click_handler([], action);
			}
		}, form_id + " .shippingaddresssameasbilling[data-link-action]");
	})();

	/*
	 * Addresses : Shipping address same as billing
	 * -------------------------------------------------------
	 *
	 */
	var addresschange_form = (function() {

		var form_id = scope + " .jsaddressform form#saveaddress-form";

		jQuery(".js-country").prop("onchange", null).attr("onchange", null);
		jQuery(document).off("change", ".js-country");
		jQuery('body').off('change',  '.js-country');
		jQuery(document).on({
			change: function (e) {

				var current_values = [];

				jQuery(form_id + " input").each( function() {
					current_values[jQuery(this).prop("name")] = jQuery(this).val();
				});

				var action = 'addressform';
				var address_id = jQuery(form_id + " input[name=id_address]").val();
				var country_id = jQuery(form_id + " select[name=id_country]").val();
				var address_type = jQuery(form_id + " .saveaddress[data-link-action=saveaddress][data-address-type]").data("address-type");
				if (typeof(address_type) === "undefined") {
					address_type = jQuery(form_id + " [name=address_type]").val();
				}

				var use_same_address = jQuery(form_id + " input[name=use_same_address]").prop("checked");

				var data = [
					{name: "id_address", value: address_id},
					{name: "id_country", value: country_id},
					{name: "address_type", value: address_type},
					{name: "use_same_address", value: use_same_address}
				];

				link_click_handler(data, action, function () {
					jQuery(form_id + " input").each( function () {
						jQuery(this).val( current_values[jQuery(this).prop('name')] );
					});
				});
			}
		}, form_id + " select.js-country");
	})();

	/*
	 * Shipping Method : Select carrier
	 * -------------------------------------------------------
	 *
	 */
	var shippingmethod_select = (function() {

		var form_id = scope + " #js-delivery";

		jQuery(document).on({
			change: function () {
				form_click_handler(form_id, "setdeliveryoption", update_checkout_summary);
			}
		}, form_id + " [id^=delivery_option]");
	})();

	/*
	 * Shipping Method : Add/Remove gift wrapped
	 * -------------------------------------------------------
	 *
	 */
	var gift_select = (function() {

		var form_id = scope + " #js-delivery";

		jQuery(document).on({
			change: function (e) {

				if (e.target.checked == true) {
					jQuery(form_id + ' #gift').addClass('in');
				}
				else {
					jQuery(form_id + ' #gift').removeClass('in');
				}

				form_click_handler(form_id, "setdeliveryoption", update_checkout_summary);
			}
		}, form_id + " input[name=gift]");
	})();

	/*
	 * Shipping Method : Set gift message
	 * -------------------------------------------------------
	 *
	 */
	var giftmessage_select = (function() {

		var form_id = scope + " #js-delivery";

		jQuery(document).on({
			change: function (e) {
				form_click_handler(form_id, "setdeliveryoption", update_checkout_summary);
			}
		}, form_id + " #gift_message");
	})();

	/*
	 * Shipping Method : Set recyclable
	 * -------------------------------------------------------
	 *
	 */
	var setrecyclable_select = (function() {

		var form_id = scope + " #js-delivery";

		jQuery(document).on({
			change: function (e) {
				form_click_handler(form_id, "setdeliveryoption", update_checkout_summary);
			}
		}, form_id + " [name=recyclable]");
	})();

	/*
	 * Payment : Validation
	 * -------------------------------------------------------
	 *
	 */
	var payment_step_validation = (function() {

		var form_id = scope + " .spc_content_wrapper";
		var iagree_selected = false;
		var payment_selected = false;

		var update_order_button_status = function() {
			if (iagree_selected && payment_selected) {
				enable_submit_button();
				update_progress_bar_status(true);
			}
			else {
				disable_submit_button();
				update_progress_bar_status(false);
			}
		};

		var disable_submit_button = function() {
			jQuery(form_id + " #submit_order_button").attr("disabled", "disabled");
		};

		var enable_submit_button = function() {
			jQuery(form_id + " #submit_order_button").removeAttr("disabled");
		};

		// i agree selected
		jQuery(document).on({
			change: function (e) {
				iagree_selected = e.target.checked ? true : false;

				update_order_button_status();
			}
		}, form_id + " input[id^=conditions_to_approve]");

		// payment method selected
		jQuery(document).on({
			change: function (e) {

				var selected = jQuery(form_id + " input[id^=payment-option]:checked");
				var additional_info_id = "#" + selected.attr("id") + "-additional-information";

				// hide all other additional info sections
				jQuery(form_id + " .payment-options .additional-information").addClass("ps-hidden");

				// display currently selected additional info section
				jQuery(form_id + " .payment-options " + additional_info_id).removeClass("ps-hidden");

				payment_selected = (selected.length > 0) ? true : false;

				update_order_button_status();
			}
		}, form_id + " input[id^=payment-option]");

		// payment confirm
		jQuery(document).on({
			click: function (e) {
				e.preventDefault();

				var selected_id = jQuery(form_id + " .payment-options input[id^=payment-option][name=payment-option]:checked").attr("id");
				if (typeof(selected_id) !== "string") {
					return;
				}

				var selected_url = jQuery(form_id + " #pay-with-" + selected_id + "-form #payment-form").attr("action");

				disable_submit_button();

				validate_checkout(selected_url);
			}
		}, form_id + " #submit_order_button");

		// validate checkout
		var validate_checkout = function(goto_url) {

			var params = [];

			add_ajax_required_params(params, 'validatecheckout');

			ajax_params.data = params;

			jQuery.ajax(ajax.url, ajax_params).done( function(resp) {
				// console.dir(resp);

				if (typeof(resp.status) === "string") {
					if (resp.status == 'ok') {
						document.location.href = goto_url;
					}
					else if (resp.status == 'err') {
						document.location.reload(true);
					}
				}
			}).fail(ajax_fail_handler);
		};
	})();

})(jQuery);
