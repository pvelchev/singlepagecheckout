{*
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
*}
{extends file="{$frame_local_path}template/helpers/options/options.tpl"}

{block name="after"}

	<div class="modal fade" id="preview_layout_modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{l s='Preview layout' mod='nlvoucher'}</h4>
				</div>
				<div class="modal-body">
					<iframe id="preview_frame" style="width: 100%; height: 700px" frameborder="0"></iframe>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{l s='Close' mod='nlvoucher'}</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<script type="text/javascript">
		$("#preview_email_layout").on('click',function(e){
			e.preventDefault();
			$("#preview_frame").attr('src','{$link->getAdminLink('AdminModules')}&configure=nlvoucher&ajax=1&action=previewlayout&layout='+$("#NL_MAIL_LAYOUT").val());
			$("#preview_layout_modal").modal('show');
		});
	</script>
{/block}

{block name="script"}
<script type="text/javascript">

	(function() {

		/***********************************************/
		/* Use the default color scheme */

		function toggle_color_scheme_items(state)
		{
			if (state) {
				$("[id^=conf_id_spciw_color_scheme_]").closest(".form-group").hide();
			}
			else {
				$("[id^=conf_id_spciw_color_scheme_]").closest(".form-group").show();
			}
		}

		// hide initialy
		if ($("#spciw_use_the_default_color_scheme_on").prop("checked")) {
			toggle_color_scheme_items(true);
		}

		$("#spciw_use_the_default_color_scheme_on, #spciw_use_the_default_color_scheme_off").on({
			change : function() {

				var state = $(this).val();

				switch (state) {
					case '0':
						toggle_color_scheme_items(false);
						break;
					case '1':
					default:
						toggle_color_scheme_items(true);
						break;
				}
			}
		});

		/***********************************************/
		/* Display the content of last 2 steps as info */

		var last_two_steps_obj2 = $("#conf_id_spciw_display_the_content_of_last_2_steps_as_info").closest(".form-group");

		if ($("#spciw_hide_last_3_checkout_steps_on").prop("checked")) {
			last_two_steps_obj2.hide();
		}

		$("#spciw_hide_last_3_checkout_steps_on, #spciw_hide_last_3_checkout_steps_off").on({
			change : function() {

				var state = $(this).val();

				switch (state) {
					case '0':
						last_two_steps_obj2.show();
						break;
					case '1':
					default:
						last_two_steps_obj2.hide();
						break;
				}
			}
		});

		/**************************************************************/
		/* Enable or Disable the integrated Next Level Voucher module */

		var nl_obj1 = $("#conf_id_spciw_nl_cart_cond").closest(".form-group");
		var nl_obj2 = $("#conf_id_spciw_nl_mail_layout").closest(".form-group");
		var nl_obj3 = $(".inixframe form#spciw_nl_condition");

		var nl_show = function () {
			nl_obj1.show();
			nl_obj2.show();
			nl_obj3.show();
		};

		var nl_hide = function () {
			nl_obj1.hide();
			nl_obj2.hide();
			nl_obj3.hide();
		};

		if ($("#spciw_nl_enabled_on").prop("checked")) {
			nl_show();
		}
		else {
			nl_hide();
		}

		$("#spciw_nl_enabled_on, #spciw_nl_enabled_off").on({
			change : function() {

				var state = $(this).val();

				switch (state) {
					case '0':
						nl_hide();
						break;
					case '1':
					default:
						nl_show();
						break;
				}
			}
		});

	})();

</script>
{/block}
