{*
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
*}
<style type="text/css">
	#spciw.theme-slice {
		font-size: 13px;
		line-height: 17px;
		padding: 0;
		box-sizing: border-box;
		display: flex;
		flex-direction: row;
	}
	#spciw.theme-slice.f_column {
		flex-direction: column;
	}
	#spciw.theme-slice section#CartSummary_content {
		margin-bottom: 0;
		padding: 15px;
	}
	#spciw.theme-slice section.checkout-step.-current {
		padding: 15px;
	}
	#spciw.theme-slice .main_content {
		position: relative;
	}
	#spciw.theme-slice .col_wrapper {
		padding: 0;
		display: block;
	}
	#spciw.theme-slice .as_flex {
		display: flex;
	}
	#spciw.theme-slice .fw_space {
		flex: 0 0 5px;
	}
	#spciw.theme-slice .fw_auto_full {
		flex: 1 0 auto;
	}
	#spciw.theme-slice .fw_auto {
		flex: 0 0 auto;
	}
	#spciw.theme-slice .fw_25 {
		flex: 0 0 25%;
	}
	#spciw.theme-slice .fw_35 {
		flex: 0 0 35%;
	}
	#spciw.theme-slice .fw_30 {
		flex: 0 0 30%;
	}
	#spciw.theme-slice .fw_50 {
		flex: 0 0 50%;
	}
	#spciw.theme-slice .fw_60 {
		flex: 0 0 60%;
	}
	#spciw.theme-slice .fw_65 {
		flex: 0 0 65%;
	}
	#spciw.theme-slice .fw_70 {
		flex: 0 0 70%;
	}
	#spciw.theme-slice .fw_75 {
		flex: 0 0 75%;
	}
	#spciw.theme-slice .fw_100 {
		flex: 0 0 100%;
	}
	#spciw.theme-slice .fl_auto {
		flex: 0 0 auto;
	}
	#spciw.theme-slice .step_wrapper {
		/*margin-right: 1px;*/
		/*border-right: 1px solid transparent;*/
	}
	#spciw.theme-slice .step_title {
		font-size: 15px;
		font-weight: bold;
		line-height: 19px;
		text-transform: uppercase;
	}
	#spciw.theme-slice .step_wrapper .step_title + hr {
		margin: 6px 0 0 0;
		padding: 0;
	}
	#spciw.theme-slice p {
		font-size: 13px;
		line-height: 17px;
		margin: 6px 0 6px 0;
	}
	#spciw.theme-slice .bold {
		font-weight: bold;
	}
	#spciw.theme-slice .pl5 {
		padding-left: 5px;
	}
	#spciw.theme-slice .pl10 {
		padding-left: 10px;
	}
	#spciw.theme-slice .ml5 {
		margin-left: 5px;
	}
	#spciw.theme-slice .mt5 {
		margin-top: 5px;
	}
	#spciw.theme-slice .mb0 {
		margin-bottom: 0;
	}
	#spciw.theme-slice .mb5 {
		margin-bottom: 5px;
	}
	#spciw.theme-slice .mb10 {
		margin-bottom: 10px;
	}
	#spciw.theme-slice .mt10 {
		margin-top: 10px;
	}
	#spciw.theme-slice .mt15 {
		margin-top: 15px;
	}
	#spciw.theme-slice .mr15 {
		margin-right: 15px;
	}
	#spciw.theme-slice .field_wrapper {}
	#spciw.theme-slice .field_wrapper .field_title {
		font-size: 13px;
		line-height: 19px;
		font-weight: bold;
	}
	#spciw.theme-slice .field_wrapper .user_input {}
	#spciw.theme-slice .field_wrapper .user_input input {
		width: 100%;
		padding: 2px 0;
		font-size: 13px;
		line-height: 19px;
		font-weight: normal;
		background-color: transparent;
		border-top-width: 0;
		border-left-width: 0;
		border-right-width: 0;
		border-bottom-width: 1px;
		border-bottom-color: {$color_scheme.input_field_underline_inactive};
	}
	#spciw.theme-slice .field_wrapper .user_input input:hover,
	#spciw.theme-slice .field_wrapper .user_input input:focus {
		border-bottom-color: {$color_scheme.input_field_underline_active};
	}
	#spciw.theme-slice .field_wrapper .user_input input {
		outline-width: 0;
	}
	#spciw.theme-slice .field_wrapper .user_input .input_label {
		font-size: 13px;
		line-height: 19px;
		float: left;
	}
	#spciw.theme-slice .check_icon {
		position: relative;
		width: 12px;
		height: 12px;
		margin-top: 2px;
		margin-right: 4px;
		border: 2px solid {$color_scheme.icon_foreground};
		background-color: transparent;
		float: left;
	}

	#spciw.theme-slice .check_icon .icon_body {
		position: absolute;
		top: 0px;
		left: 0px;
		width: 8px;
		height: 8px;
		border: 1px solid {$color_scheme.icon_background};
		background-color: transparent;
		float: left;
	}
	#spciw.theme-slice .check_icon .icon_body.active {
		background-color: {$color_scheme.icon_foreground};
	}
	#spciw.theme-slice .field_wrapper .user_input .input_label + .check_icon {
		margin-left: 6px;
	}
	#spciw.theme-slice .radio_icon {
		position: relative;
		width: 12px;
		height: 12px;
		margin-top: 3px;
		margin-right: 4px;
		border: 2px solid {$color_scheme.icon_foreground};
		border-radius: 8px;
		background-color: transparent;
		float: left;
	}
	#spciw.theme-slice .radio_icon.radio_icon_address {
		margin: 2px 6px 0 0;
	}
	#spciw.theme-slice .radio_icon .icon_body {
		position: absolute;
		top: 0px;
		left: 0px;
		width: 8px;
		height: 8px;
		border: 1px solid {$color_scheme.icon_background};
		border-radius: 6px;
		background-color: transparent;
		float: left;
	}
	#spciw.theme-slice .radio_icon .icon_body.active {
		background-color: {$color_scheme.icon_foreground};
	}
	#spciw.theme-slice .field_wrapper .user_input .input_label + .radio_icon {
		margin-left: 6px;
	}
	#spciw.theme-slice .field_wrapper .user_input .user_selection {
		cursor: pointer;
		float: left;
	}
	#spciw.theme-slice .field_wrapper .user_input .user_selection + .user_selection {
		margin-left: 6px;
	}
	#spciw.theme-slice .user_credential_links {
		font-size: 13px;
		line-height: 17px;
		display: block;
	}
	#spciw.theme-slice .basic_info {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-slice .show_hide_pass {
		text-transform: uppercase;
		font-size: 13px;
		font-weight: normal;
		line-height: 17px;
	}
	#spciw.theme-slice .field_wrapper .user_input input.w_80 {
		width: 80%;
	}
	#spciw.theme-slice .f_left {
		float: left;
	}
	#spciw.theme-slice .f_right {
		float: right;
	}
	#spciw.theme-slice .fnt_italic {
		font-style: italic;
	}
	#spciw.theme-slice .clearfix {
		width: auto;
		height: 0;
		margin: 0;
		padding: 0;
		clear: both;
		display: block;
	}
	#spciw.theme-slice .button_primary {
		padding: 4px 8px;
		font-size: 13px;
		outline-width: 0;
	}
	#spciw.theme-slice .hidden {
		display: none;
	}
	#spciw.theme-slice .forgot_password {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-slice .connected_as {
		font-size: 13px;
		line-height: 17px;
		margin: 10px 0 0 0;
	}
	#spciw.theme-slice .connected_as small {
		font-size: 11px;
		line-height: 15px;
	}
	#spciw.theme-slice .page_errors {
		margin-top: 2px;
		margin-bottom: 0px;
		padding: 6px 10px;
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-slice .complete_step {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-slice .basic_info {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-slice .basic_link {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-slice .cancel_link {
		margin-top: 4px;
		margin-left: 5px;
	}
	#spciw.theme-slice #address_country,
	#spciw.theme-slice #address_state {
		background-color: inherit;
		border-width: 1px;
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-slice #customer_invoice_addresses,
	#spciw.theme-slice #customer_delivery_addresses {
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
	}
	#spciw.theme-slice .address_item {
		position: relative;
		flex: 0 0 auto;
		cursor: pointer;
		padding: 10px;
		display: flex;
		flex-direction: column;
	}
	#spciw.theme-slice .address_item.active,
	#spciw.theme-slice .address_item:hover {
		background-color: {$color_scheme.selected_item_background};
	}
	#spciw.theme-slice .address_item .body {
		flex: 1 0 auto;
	}
	#spciw.theme-slice .address_item .actions {
		flex: 0 0 auto;
	}
	#spciw.theme-slice .address_item .actions {
	}
	#spciw.theme-slice .address_alias {
		font-size: 13px;
		line-height: 17px;
		font-weight: bold;
	}
	#spciw.theme-slice .address_formatted {
		padding-top: 4px;
		/*padding-left: 18px;*/
	}
	#spciw.theme-slice .address_type {
		font-size: 15px;
		line-height: 19px;
	}
	#spciw.theme-slice .material_icon {
		font-size: 13px;
		line-height: 13px;
	}
	#spciw.theme-slice .dib {
		display: inline-block;
	}
	#spciw.theme-slice .is_link {
		cursor: pointer;
	}
	#spciw.theme-slice .addressaction + .addressaction {
		margin-left: 6px;
	}
	#spciw.theme-slice .missing_methods {
		padding: 6px 10px;
	}
	#spciw.theme-slice .shipping_options_wrapper {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-slice .carrier_extra_content {
		padding: 0 15px;
	}
	#spciw.theme-slice .shipping_option {
		padding: 10px 0;
		display: flex;
		flex-direction: row;
		cursor: pointer;
	}
	#spciw.theme-slice .shipping_option:hover,
	#spciw.theme-slice .shipping_option.active {
		background-color: {$color_scheme.selected_item_background};
	}
	#spciw.theme-slice .shipping_option .option_column {
		/*height: 40px;*/
		vertical-align: middle;
		flex: 1 0 auto;
		display: flex;
		align-items: center;
		/*justify-content: flex-start;*/
	}
	#spciw.theme-slice .shipping_option .option_column.tide {
		flex: 0 0 auto;
		padding-right: 5px;
	}
	#spciw.theme-slice .shipping_option .option_column.select_icon {
		flex: 0 0 40px;
		padding: 0 15px;
		display: flex;
		flex-direction: column;
		justify-content: center;
	}
	#spciw.theme-slice .shipping_option .option_column.select_icon .radio_icon {
		margin: 0;
		flex: 0 0 auto;
	}
	#spciw.theme-slice .shipping_option .option_column.carrier_name {
		flex: 0 0 20%;
	}
	#spciw.theme-slice .shipping_option .option_column.carrier_delay {
		flex: 0 0 25%;
	}
	#spciw.theme-slice .shipping_option .option_column.carrier_price {
		flex: 0 0 20%;
	}
	#spciw.theme-slice .shipping_option .option_column.f_free {
		flex: 1 0 auto;
	}
	#spciw.theme-slice .payment_methods_wrapper {
		display: block;
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-slice .payment_option {
		padding: 0;
		display: flex;
		flex-direction: row;
	}
	#spciw.theme-slice .payment_option .option_column {
		min-height: 25px;
		flex: 0 1 auto;
	}
	#spciw.theme-slice .payment_option .option_column.select_icon {
		flex: 0 0 30px;
		margin-top: 7px;
		padding: 0 10px;
	}
	#spciw.theme-slice .payment_option .option_column.payment_name {
		line-height: 25px;
	}
	#spciw.theme-slice .payment_option .option_column.select_icon .radio_icon {
		margin: 0;
	}
	#spciw.theme-slice .payment_option .select_icon .radio_icon,
	#spciw.theme-slice .payment_option .payment_name_wrapper {
		cursor: pointer;
	}
	#spciw.theme-slice #payment_methods_submit_button {
		padding: 6px 12px;
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-slice .overlay {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: transparent;
		z-index: 909090;
	}
	#spciw.theme-slice .grey-out {
		filter: grayscale(100%);
	}
	#BlockReassurance_wrapper {
		font-size: 13px;
		line-height: 17px;
		display: inline-block;
	}
	#BlockReassurance_wrapper * {
		font-size: inherit;
		line-height: inherit;
	}
	#spciw.theme-slice .cart_summary_top {
		position: relative;
	}
	#spciw.theme-slice .cart_summary_line {
		width: 100%;
		display: table;
		clear: both;
	}
	#spciw.theme-slice .cart_summary_line .label {
		float: left;
	}
	#spciw.theme-slice .cart_summary_line .value {
		float: right;
	}
	#spciw.theme-slice .lh24 {
		line-height: 24px;
	}
	#spciw.theme-slice .error_box {
		padding: 6px 10px;
	}
	.progress {
		height: 24px;
		margin-bottom: 14px;
		display: block;
		background-color: {$color_scheme.progress_bar_background};
	}
	.progress .progress-bar {
		height: 24px;
		line-height: 24px;
		font-size: 15px;
		text-align: center;
		float: left;
		color: {$color_scheme.progress_bar_text_color};
		/*background-color: #000;*/
		/*background-color: #337AB7;*/
		/*background-color: #2FB5D2;*/
		background-color: {$color_scheme.progress_bar_foreground};
	}
	/*
	#spciw.theme-slice ::placeholder {
		color: rgba(0, 0, 0, 0.65);
	}
	*/
	#spciw.theme-slice .totals {
		font-size: 15px;
		line-height: 25px;
	}
	#spciw.theme-slice .modal-body {
		padding: 15px;
	}
	#spciw.theme-slice .cs_product_list {}
	#spciw.theme-slice .cs_product_list .product_list_item_wrapper {
		display: flex;
		flex-direction: row;
	}
	#spciw.theme-slice .cs_product_list .product_list_item_wrapper .item {
		flex: 0 0 auto;
	}
	#spciw.theme-slice .cs_product_list .product_list_item_wrapper .item_full {
		flex: 1 0 auto;
	}
	#spciw.theme-slice .dflex {
		display: flex;
	}
	#spciw.theme-slice .pt5 {
		padding-top: 5px;
	}
</style>
