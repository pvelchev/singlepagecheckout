{*
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
*}
{if $display_as_info}
<div class="overlay"></div>
{/if}

<div id="hook-display-before-carrier">
	{$hookDisplayBeforeCarrier nofilter}
</div>

{if $delivery_options|count}
	<div class="shipping_options_wrapper mt10" data-delivery-address-id="{$id_address}">
		{foreach $delivery_options item=carrier key=carrier_id}
		<div class="shipping_option mb5 {if $delivery_option == $carrier_id}active{/if}" data-carrier-id="{$carrier_id}">
			<div class="option_column select_icon">
				<div class="radio_icon">
					<div class="icon_body {if $delivery_option == $carrier_id}active{/if}"></div>
				</div>
			</div>
			{if ! empty($carrier.logo)}
			<div class="option_column f_free"><img src="{$carrier.logo}" alt="{$carrier.name}"></div>
			<div class="option_column carrier_name f_free">{$carrier.name}</div>
			{else}
			<div class="option_column">{$carrier.name}</div>
			{/if}
			<div class="option_column carrier_delay">{$carrier.delay}</div>
			<div class="option_column carrier_price">{$carrier.price}</div>
		</div>
		{if $delivery_option == $carrier_id && ! empty($carrier.extraContent)}
		<div class="carrier_extra_content">{$carrier.extraContent nofilter}</div>
		{/if}
		{/foreach}
	</div>

	{if $recyclablePackAllowed}
	<div class="field_wrapper mt15">
		<div class="user_input">
			<div id="recycled_packaging_wrapper" class="user_selection check">
				<div class="check_icon">
					<div id="recycled_packaging_selected" class="icon_body {if $recyclable}active{/if}"></div>
				</div>
				<div class="basic_info f_left">{l s='I would like to receive my order in recycled packaging.' d='Shop.Theme.Checkout'}</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	{/if}

	{if $gift.allowed}
	<div class="field_wrapper mt10">
		<div class="user_input">
			<div id="gift_selected_wrapper" class="user_selection check">
				<div class="check_icon">
					<div id="gift_selected" class="icon_body {if $gift.isGift}active{/if}"></div>
				</div>
				<div class="basic_info f_left mb5">{$gift.label}</div>
			</div>
			<div class="clearfix"></div>
			<div id="gift_message_wrapper" class="{if ! $gift.isGift}hidden{/if}">
				<div class="basic_info fnt_italic">{l s='If you\'d like, you can add a note to the gift:' d='Shop.Theme.Checkout'}</div>
				<input id="gift_message" type="text" name="gift_message" maxlength="128" value="{$gift.message}">
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	{/if}

	{* PS 1.7.X support only? *}
	{*
	<div class="field_wrapper mt15">
		<div class="field_title basic_info">{l s='If you would like to add a comment about your order, please write it in the field below.' d='Shop.Theme.Checkout'}</div>
		<div class="user_input">
			<input id="delivery_message" type="text" maxlength="256">
		</div>
	</div>
	*}

{else}
	<p class="alert alert-danger basic_info missing_methods">{l s='Unfortunately, there are no carriers available for your delivery address.' d='Shop.Theme.Checkout'}</p>
{/if}

<div id="hook-display-after-carrier">
	{$hookDisplayAfterCarrier nofilter}
</div>

<div id="extra_carrier"></div>
