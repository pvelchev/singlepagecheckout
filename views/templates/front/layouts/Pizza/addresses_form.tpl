{*
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
*}
{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/address_header_info.tpl"}

<input type="hidden" name="CustomerAddresses_id_customer" value="{if isset($customer_id)}{$customer_id}{/if}">
<input type="hidden" name="CustomerAddresses_id_address" value="{if isset($address_id)}{$address_id}{/if}">
<input type="hidden" name="CustomerAddresses_is_delivery_address" value="{$is_delivery_address}">

<div class="field_wrapper mt15">
	<div class="field_title">{l s='First name'} <span>*</span></div>
	<div class="user_input">
		<input type="text" name="CustomerAddresses_firstname" maxlength="32" value="{if ! empty($address_firstname)}{$address_firstname}{/if}">
	</div>
</div>
{if isset($form_errors.firstname) && $form_errors.firstname|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.firstname}
{/if}

<div class="field_wrapper mt15">
	<div class="field_title">{l s='Last name'} <span>*</span></div>
	<div class="user_input">
		<input type="text" name="CustomerAddresses_lastname" maxlength="32" value="{if ! empty($address_lastname)}{$address_lastname}{/if}">
	</div>
</div>
{if isset($form_errors.lastname) && $form_errors.lastname|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.lastname}
{/if}

{if $display_field_company == 1}
<div class="field_wrapper mt15">
	<div class="field_title">{l s='Company'}</div>
	<div class="user_input">
		<input type="text" name="CustomerAddresses_company" maxlength="32" value="{if ! empty($address_company)}{$address_company}{/if}">
	</div>
</div>
{if isset($form_errors.company) && $form_errors.company|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.company}
{/if}
{/if}

{if $display_field_vat_number == 1}
<div class="field_wrapper mt15">
	<div class="field_title">{l s='VAT Number'}</div>
	<div class="user_input">
		<input type="text" name="CustomerAddresses_vat_number" maxlength="32" value="{if ! empty($address_vat_number)}{$address_vat_number}{/if}">
	</div>
</div>
{if isset($form_errors.vat_number) && $form_errors.vat_number|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.vat_number}
{/if}
{/if}

<div class="field_wrapper mt15">
	<div class="field_title">{l s='Address'} <span>*</span></div>
	<div class="user_input">
		<input type="text" name="CustomerAddresses_address" maxlength="128" value="{if ! empty($address_address)}{$address_address}{/if}">
	</div>
</div>
{if isset($form_errors.address1) && $form_errors.address1|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.address1}
{/if}

{if $display_field_address_complement == 1}
<div class="field_wrapper mt15">
	<div class="field_title">{l s='Address Complement'}</div>
	<div class="user_input">
		<input type="text" name="CustomerAddresses_address_complement" maxlength="128" value="{if ! empty($address_address_complement)}{$address_address_complement}{/if}">
	</div>
</div>
{if isset($form_errors.address2) && $form_errors.address2|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.address2}
{/if}
{/if}

<div class="field_wrapper mt15">
	<div class="field_title">{l s='Zip/Postal Code'} <span>*</span></div>
	<div class="user_input">
		<input type="text" name="CustomerAddresses_zip_postal_code" maxlength="16" value="{if ! empty($address_zip_postal_code)}{$address_zip_postal_code}{/if}">
	</div>
</div>
{if isset($form_errors.postcode) && $form_errors.postcode|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.postcode}
{/if}

<div class="field_wrapper mt15">
	<div class="field_title">{l s='City'} <span>*</span></div>
	<div class="user_input">
		<input type="text" name="CustomerAddresses_city" maxlength="32" value="{if ! empty($address_city)}{$address_city}{/if}">
	</div>
</div>
{if isset($form_errors.city) && $form_errors.city|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.city}
{/if}

<div class="field_wrapper mt15">
	<div class="field_title">{l s='Country'} <span>*</span></div>
	<div class="user_input">
		<select id="address_country" class="mt5">
			<option value="0" {if $address_country_selected > 0}disabled{/if}>-- please choose --</option>
			{foreach from=$address_countries item="country"}
				<option value="{$country.id_country}" {if $address_country_selected > 0 && $address_country_selected == $country.id_country}selected{/if}>{$country.name}</option>
			{/foreach}
		</select>
	</div>
</div>
{if isset($form_errors.id_country) && $form_errors.id_country|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.id_country}
{/if}

{if $address_country_selected > 0 && isset($address_states) && $address_states|count > 0}
<div class="field_wrapper mt15">
	<div class="field_title">{l s='State'} <span>*</span></div>
	<div class="user_input">
		<select id="address_state" class="mt5">
			<option value="0" {if $address_state_selected > 0}selected{/if}>-- please choose --</option>
			{foreach from=$address_states item="state"}
				<option value="{$state.id_state}" {if $address_state_selected > 0 && $address_state_selected == $state.id_state}selected{/if}>{$state.name}</option>
			{/foreach}
		</select>
	</div>
</div>
{/if}

{if $display_field_phone == 1}
<div class="field_wrapper mt15">
	<div class="field_title">{l s='Phone'}</div>
	<div class="user_input">
		<input type="text" name="CustomerAddresses_phone" maxlength="64" value="{if ! empty($address_phone)}{$address_phone}{/if}">
	</div>
</div>
{if isset($form_errors.phone) && $form_errors.phone|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.phone}
{/if}
{/if}

{*
{if $display_same_address_option}
<div class="field_wrapper mt15">
	<div class="user_input">
		<div class="user_selection check">
			<div class="check_icon">
				<div id="use_same_address" class="icon_body {if $use_same_address}active{/if}"></div>
			</div>
			<div class="basic_info f_left mb5">{l s='Use this address for invoice too' d='Shop.Theme.Checkout'}</div>
		</div>

		<div class="clearfix"></div>
	</div>
</div>
{/if}
*}

<div class="mt10">
	<button class="saveaddress btn btn-primary button_primary">{l s='Save' d='Shop.Theme.Actions'}</button>
	{if $display_cancel_button}
	<a href="#" class="editcancel basic_link cancel_link">{l s='Cancel' d='Shop.Theme.Actions'}</a>
	{/if}
</div>
