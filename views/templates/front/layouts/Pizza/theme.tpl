{*
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
*}
<style type="text/css">
	#spciw.theme-pizza {
		font-size: 13px;
		line-height: 17px;
		padding: 0;
		box-sizing: border-box;
		display: flex;
		flex-direction: row;
	}
	#spciw.theme-pizza section#CartSummary_content,
	#spciw.theme-pizza section.checkout-step.-current {
		padding: 15px;
	}
	#spciw.theme-pizza .main_content {
		position: relative;
	}
	#spciw.theme-pizza .col_wrapper {
		padding: 0;
		display: block;
	}
	#spciw.theme-pizza .as_flex {
		display: flex;
	}
	#spciw.theme-pizza .fw_space {
		flex: 0 0 5px;
	}
	#spciw.theme-pizza .fw_auto_full {
		flex: 1 0 auto;
	}
	#spciw.theme-pizza .fw_auto {
		flex: 0 0 auto;
	}
	#spciw.theme-pizza .fw_25 {
		flex: 0 0 25%;
	}
	#spciw.theme-pizza .fw_35 {
		flex: 0 0 35%;
	}
	#spciw.theme-pizza .fw_30 {
		flex: 0 0 30%;
	}
	#spciw.theme-pizza .fw_50 {
		flex: 0 0 50%;
	}
	#spciw.theme-pizza .fw_60 {
		flex: 0 0 60%;
	}
	#spciw.theme-pizza .fw_65 {
		flex: 0 0 65%;
	}
	#spciw.theme-pizza .fw_70 {
		flex: 0 0 70%;
	}
	#spciw.theme-pizza .fw_75 {
		flex: 0 0 75%;
	}
	#spciw.theme-pizza .fw_100 {
		flex: 0 0 100%;
	}
	#spciw.theme-pizza .fl_auto {
		flex: 0 0 auto;
	}
	#spciw.theme-pizza .step_wrapper {
		/*margin-right: 1px;*/
		/*border-right: 1px solid transparent;*/
	}
	#spciw.theme-pizza .step_title {
		font-size: 15px;
		font-weight: bold;
		line-height: 19px;
		text-transform: uppercase;
	}
	#spciw.theme-pizza .step_wrapper .step_title + hr {
		margin: 6px 0 0 0;
		padding: 0;
	}
	#spciw.theme-pizza p {
		font-size: 13px;
		line-height: 17px;
		margin: 6px 0 6px 0;
	}
	#spciw.theme-pizza .bold {
		font-weight: bold;
	}
	#spciw.theme-pizza .pl5 {
		padding-left: 5px;
	}
	#spciw.theme-pizza .pl10 {
		padding-left: 10px;
	}
	#spciw.theme-pizza .ml5 {
		margin-left: 5px;
	}
	#spciw.theme-pizza .mt5 {
		margin-top: 5px;
	}
	#spciw.theme-pizza .mb0 {
		margin-bottom: 0;
	}
	#spciw.theme-pizza .mb5 {
		margin-bottom: 5px;
	}
	#spciw.theme-pizza .mb10 {
		margin-bottom: 10px;
	}
	#spciw.theme-pizza .mt10 {
		margin-top: 10px;
	}
	#spciw.theme-pizza .mt15 {
		margin-top: 15px;
	}
	#spciw.theme-pizza .mr15 {
		margin-right: 15px;
	}
	#spciw.theme-pizza .field_wrapper {}
	#spciw.theme-pizza .field_wrapper .field_title {
		font-size: 13px;
		line-height: 19px;
		font-weight: bold;
	}
	#spciw.theme-pizza .field_wrapper .user_input {}
	#spciw.theme-pizza .field_wrapper .user_input input {
		width: 100%;
		padding: 2px 0;
		font-size: 13px;
		line-height: 19px;
		font-weight: normal;
		background-color: transparent;
		border-top-width: 0;
		border-left-width: 0;
		border-right-width: 0;
		border-bottom-width: 1px;
		border-bottom-color: {$color_scheme.input_field_underline_inactive};
	}
	#spciw.theme-pizza .field_wrapper .user_input input:hover,
	#spciw.theme-pizza .field_wrapper .user_input input:focus {
		border-bottom-color: {$color_scheme.input_field_underline_active};
	}
	#spciw.theme-pizza .field_wrapper .user_input input {
		outline-width: 0;
	}
	#spciw.theme-pizza .field_wrapper .user_input .input_label {
		font-size: 13px;
		line-height: 19px;
		float: left;
	}
	#spciw.theme-pizza .check_icon {
		position: relative;
		width: 12px;
		height: 12px;
		margin-top: 2px;
		margin-right: 4px;
		border: 2px solid {$color_scheme.icon_foreground};
		background-color: transparent;
		float: left;
	}

	#spciw.theme-pizza .check_icon .icon_body {
		position: absolute;
		top: 0px;
		left: 0px;
		width: 8px;
		height: 8px;
		border: 1px solid {$color_scheme.icon_background};
		background-color: transparent;
		float: left;
	}
	#spciw.theme-pizza .check_icon .icon_body.active {
		background-color: {$color_scheme.icon_foreground};
	}
	#spciw.theme-pizza .field_wrapper .user_input .input_label + .check_icon {
		margin-left: 6px;
	}
	#spciw.theme-pizza .radio_icon {
		position: relative;
		width: 12px;
		height: 12px;
		margin-top: 3px;
		margin-right: 4px;
		border: 2px solid {$color_scheme.icon_foreground};
		border-radius: 8px;
		background-color: transparent;
		float: left;
	}
	#spciw.theme-pizza .radio_icon.radio_icon_address {
		margin: 2px 6px 0 0;
	}
	#spciw.theme-pizza .radio_icon .icon_body {
		position: absolute;
		top: 0px;
		left: 0px;
		width: 8px;
		height: 8px;
		border: 1px solid {$color_scheme.icon_background};
		border-radius: 6px;
		background-color: transparent;
		float: left;
	}
	#spciw.theme-pizza .radio_icon .icon_body.active {
		background-color: {$color_scheme.icon_foreground};
	}
	#spciw.theme-pizza .field_wrapper .user_input .input_label + .radio_icon {
		margin-left: 6px;
	}
	#spciw.theme-pizza .field_wrapper .user_input .user_selection {
		cursor: pointer;
		float: left;
	}
	#spciw.theme-pizza .field_wrapper .user_input .user_selection + .user_selection {
		margin-left: 6px;
	}
	#spciw.theme-pizza .user_credential_links {
		font-size: 13px;
		line-height: 17px;
		display: block;
	}
	#spciw.theme-pizza .basic_info {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza .show_hide_pass {
		text-transform: uppercase;
		font-size: 13px;
		font-weight: normal;
		line-height: 17px;
	}
	#spciw.theme-pizza .field_wrapper .user_input input.w_80 {
		width: 80%;
	}
	#spciw.theme-pizza .f_left {
		float: left;
	}
	#spciw.theme-pizza .f_right {
		float: right;
	}
	#spciw.theme-pizza .fnt_italic {
		font-style: italic;
	}
	#spciw.theme-pizza .clearfix {
		width: auto;
		height: 0;
		margin: 0;
		padding: 0;
		clear: both;
		display: block;
	}
	#spciw.theme-pizza .button_primary {
		padding: 4px 8px;
		font-size: 13px;
		outline-width: 0;
	}
	#spciw.theme-pizza .hidden {
		display: none;
	}
	#spciw.theme-pizza .forgot_password {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza .connected_as {
		font-size: 13px;
		line-height: 17px;
		margin: 10px 0 0 0;
	}
	#spciw.theme-pizza .connected_as small {
		font-size: 11px;
		line-height: 15px;
	}
	#spciw.theme-pizza .page_errors {
		margin-top: 2px;
		margin-bottom: 0px;
		padding: 6px 10px;
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza .complete_step {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza .basic_info {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza .basic_link {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza .cancel_link {
		margin-top: 4px;
		margin-left: 5px;
	}
	#spciw.theme-pizza #address_country,
	#spciw.theme-pizza #address_state {
		background-color: inherit;
		border-width: 1px;
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza #customer_invoice_addresses,
	#spciw.theme-pizza #customer_delivery_addresses {
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
	}
	#spciw.theme-pizza .address_item {
		position: relative;
		flex: 0 0 auto;
		cursor: pointer;
		padding: 10px;
		display: flex;
		flex-direction: column;
	}
	#spciw.theme-pizza .address_item.active,
	#spciw.theme-pizza .address_item:hover {
		background-color: {$color_scheme.selected_item_background};
	}
	#spciw.theme-pizza .address_item .body {
		flex: 1 0 auto;
	}
	#spciw.theme-pizza .address_item .actions {
		flex: 0 0 auto;
	}
	#spciw.theme-pizza .address_item .actions {
	}
	#spciw.theme-pizza .address_alias {
		font-size: 13px;
		line-height: 17px;
		font-weight: bold;
	}
	#spciw.theme-pizza .address_formatted {
		padding-top: 4px;
		/*padding-left: 18px;*/
	}
	#spciw.theme-pizza .address_type {
		font-size: 15px;
		line-height: 19px;
	}
	#spciw.theme-pizza .material_icon {
		font-size: 13px;
		line-height: 13px;
	}
	#spciw.theme-pizza .dib {
		display: inline-block;
	}
	#spciw.theme-pizza .is_link {
		cursor: pointer;
	}
	#spciw.theme-pizza .addressaction + .addressaction {
		margin-left: 6px;
	}
	#spciw.theme-pizza .missing_methods {
		padding: 6px 10px;
	}
	#spciw.theme-pizza .shipping_options_wrapper {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza .carrier_extra_content {
		padding: 0 15px;
	}
	#spciw.theme-pizza .shipping_option {
		padding: 10px 0;
		display: flex;
		flex-direction: row;
		cursor: pointer;
	}
	#spciw.theme-pizza .shipping_option:hover,
	#spciw.theme-pizza .shipping_option.active {
		background-color: {$color_scheme.selected_item_background};
	}
	#spciw.theme-pizza .shipping_option .option_column {
		/*height: 40px;*/
		vertical-align: middle;
		flex: 1 0 auto;
		display: flex;
		align-items: center;
		/*justify-content: flex-start;*/
	}
	#spciw.theme-pizza .shipping_option .option_column.tide {
		flex: 0 0 auto;
		padding-right: 5px;
	}
	#spciw.theme-pizza .shipping_option .option_column.select_icon {
		flex: 0 0 40px;
		padding: 0 15px;
		display: flex;
		flex-direction: column;
		justify-content: center;
	}
	#spciw.theme-pizza .shipping_option .option_column.select_icon .radio_icon {
		margin: 0;
		flex: 0 0 auto;
	}
	#spciw.theme-pizza .shipping_option .option_column.carrier_name {
		flex: 0 0 20%;
	}
	#spciw.theme-pizza .shipping_option .option_column.carrier_delay {
		flex: 0 0 25%;
	}
	#spciw.theme-pizza .shipping_option .option_column.carrier_price {
		flex: 0 0 20%;
	}
	#spciw.theme-pizza .shipping_option .option_column.f_free {
		flex: 1 0 auto;
	}
	#spciw.theme-pizza .payment_methods_wrapper {
		display: block;
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza .payment_option {
		padding: 0;
		display: flex;
		flex-direction: row;
	}
	#spciw.theme-pizza .payment_option .option_column {
		min-height: 25px;
		flex: 0 1 auto;
	}
	#spciw.theme-pizza .payment_option .option_column.select_icon {
		flex: 0 0 30px;
		margin-top: 7px;
		padding: 0 10px;
	}
	#spciw.theme-pizza .payment_option .option_column.payment_name {
		line-height: 25px;
	}
	#spciw.theme-pizza .payment_option .option_column.select_icon .radio_icon {
		margin: 0;
	}
	#spciw.theme-pizza .payment_option .select_icon .radio_icon,
	#spciw.theme-pizza .payment_option .payment_name_wrapper {
		cursor: pointer;
	}
	#spciw.theme-pizza #payment_methods_submit_button {
		padding: 6px 12px;
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza .overlay {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: transparent;
		z-index: 909090;
	}
	#spciw.theme-pizza .grey-out {
		filter: grayscale(100%);
	}
	#spciw.theme-pizza #BlockReassurance_wrapper {
		font-size: 13px;
		line-height: 17px;
	}
	#spciw.theme-pizza #BlockReassurance_wrapper * {
		font-size: inherit;
		line-height: inherit;
	}
	#spciw.theme-pizza .cart_summary_top {
		position: relative;
	}
	#spciw.theme-pizza .cart_summary_line {
		width: 100%;
		display: table;
		clear: both;
	}
	#spciw.theme-pizza .cart_summary_line .label {
		float: left;
	}
	#spciw.theme-pizza .cart_summary_line .value {
		float: right;
	}
	#spciw.theme-pizza .lh24 {
		line-height: 24px;
	}
	#spciw.theme-pizza .error_box {
		padding: 6px 10px;
	}
	.progress {
		height: 24px;
		margin-bottom: 14px;
		display: block;
		background-color: {$color_scheme.progress_bar_background};
	}
	.progress .progress-bar {
		height: 24px;
		line-height: 24px;
		font-size: 15px;
		text-align: center;
		float: left;
		color: {$color_scheme.progress_bar_text_color};
		/*background-color: #000;*/
		/*background-color: #337AB7;*/
		/*background-color: #2FB5D2;*/
		background-color: {$color_scheme.progress_bar_foreground};
	}
	/*
	#spciw.theme-pizza ::placeholder {
		color: rgba(0, 0, 0, 0.65);
	}
	*/
	#spciw.theme-pizza .totals {
		font-size: 15px;
		line-height: 25px;
	}
	#spciw.theme-pizza .modal-body {
		padding: 15px;
	}
	#spciw.theme-pizza .cs_product_list {}
	#spciw.theme-pizza .cs_product_list .product_list_item_wrapper {
		display: flex;
		flex-direction: row;
	}
	#spciw.theme-pizza .cs_product_list .product_list_item_wrapper .item {
		flex: 0 0 auto;
	}
	#spciw.theme-pizza .cs_product_list .product_list_item_wrapper .item_full {
		flex: 1 0 auto;
	}
</style>
