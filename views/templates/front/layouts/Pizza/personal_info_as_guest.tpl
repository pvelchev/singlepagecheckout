{*
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
*}
<div class="user_credential_links mt10">
	<a href="#checkout_order_as_guest_form" class="tablink">
		{if $guestAllowed == 1}
			{l s='Order as guest'}
		{else}
			{l s='Create an account'}
		{/if}
	</a> | <a href="#checkout_login_form" class="tablink">{l s='Sign In'}</a>
</div>

{if $page_errors|count}
	{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$page_errors}
{/if}

<div id="checkout_order_as_guest_form" class="tabcontent {if $displaySigninForm}hidden{/if}">
	<div class="field_wrapper mt10">
		<div class="field_title mb5">{l s='Social title'} <span>*</span></div>
		<div class="user_input">

			<div class="user_selection radio">
				<div class="radio_icon">
					<div class="icon_body {if ! empty($customer_gender) && $customer_gender == 1}active{/if}" data-value="1"></div>
				</div>
				<div class="input_label">Mr.</div>
			</div>

			<div class="user_selection radio">
				<div class="radio_icon">
					<div class="icon_body {if ! empty($customer_gender) && $customer_gender == 2}active{/if}" data-value="2"></div>
				</div>
				<div class="input_label">Mrs.</div>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>

	<div class="field_wrapper mt15">
		<div class="field_title">{l s='First name'} <span>*</span></div>
		<div class="user_input">
			<input type="text" name="PersonalInformation_guest_firstname" required placeholder="" maxlength="32" value="{if ! empty($customer_firstname)}{$customer_firstname}{/if}">
		</div>
	</div>
	{if isset($form_errors.firstname) && $form_errors.firstname|count}
		{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.firstname}
	{/if}

	<div class="field_wrapper mt5">
		<div class="field_title">{l s='Last name'} <span>*</span></div>
		<div class="user_input">
			<input type="text" name="PersonalInformation_guest_lastname" required placeholder="" maxlength="32" value="{if ! empty($customer_lastname)}{$customer_lastname}{/if}">
		</div>
	</div>
	{if isset($form_errors.lastname) && $form_errors.lastname|count}
		{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.lastname}
	{/if}

	<div class="field_wrapper mt5">
		<div class="field_title">{l s='E-Mail'} <span>*</span></div>
		<div class="user_input">
			<input type="email" name="PersonalInformation_guest_email" prequired laceholder="" maxlength="128" value="{if ! empty($customer_email)}{$customer_email}{/if}">
		</div>
	</div>
	{if isset($form_errors.email) && $form_errors.email|count}
		{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.email}
	{/if}

	{if $guestAllowed == 1}
	<div class="basic_info mt10">
		{l s='Create an account (optional)'}
		<br/>
		{l s='And save time on your next order!'}
	</div>
	{/if}

	<div class="field_wrapper mt10">
		<div class="field_title">{l s='Password'} {if $guestAllowed == 0}<span>*</span>{/if} <a href="#" class="show_hide_pass ml5" data-show="{l s='show'}" data-hide="{l s='hide'}">{l s='show'}</a></div>
		<div class="user_input">
			<input type="password" name="PersonalInformation_guest_password" {if $guestAllowed == 0}required{/if} placeholder="" maxlength="64">
		</div>
	</div>
	{if isset($form_errors.password) && $form_errors.password|count}
		{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.password}
	{/if}

	{if $display_field_birthday == 1}
	<div class="field_wrapper mt10">
		<div class="field_title">{l s='Birthday'}</div>
		<div class="user_input">
			<input type="text" name="PersonalInformation_guest_birthday" placeholder="{l s='E.g.'}: 05/31/1970" maxlength="64">
		</div>
	</div>
	{if isset($form_errors.birthday) && $form_errors.birthday|count}
		{include file="module:singlepagecheckoutiw/views/templates/front/layouts/Pizza/form_errors.tpl" errors=$form_errors.birthday}
	{/if}
	{/if}

	<div class="field_wrapper mt10">
		<div class="user_input">
			<div class="user_selection check">
				<div class="check_icon">
					<div class="icon_body" data-name="optin"></div>
				</div>
				<div class="basic_info f_left mb5">{l s='Receive offers from our partners'}</div>
			</div>

			<div class="clearfix"></div>

			<div class="user_selection check">
				<div class="check_icon">
					<div class="icon_body" data-name="newsletter"></div>
				</div>
				<div class="basic_info f_left">{l s='Sign up for our newsletter'}</div>
			</div>

			<div class="clearfix"></div>

			<div class="basic_info fnt_italic">{l s='You may unsubscribe at any moment. For that purpose, please find our contact info in the legal notice.'}</div>
		</div>

		<div class="clearfix"></div>
	</div>

	<div class="mt10">
		<button class="orderasguest btn btn-primary button_primary">{l s='Continue' d='Shop.Theme.Actions'}</button>
	</div>
</div>

<div id="checkout_login_form" class="tabcontent {if ! $displaySigninForm}hidden{/if}">
	<div class="field_wrapper mt10">
		<div class="field_title">{l s='E-Mail'} <span>*</span></div>
		<div class="user_input">
			<input type="email" name="PersonalInformation_signin_email" placeholder="" maxlength="128" value="{if ! empty($signin_email)}{$signin_email}{/if}">
		</div>
	</div>
	<div class="field_wrapper mt10">
		<div class="field_title">{l s='Password'} <span>*</span> <a href="#" class="show_hide_pass ml5" data-show="{l s='show'}" data-hide="{l s='hide'}">{l s='show'}</a></div>
		<div class="user_input">
			<input type="password" name="PersonalInformation_signin_password" placeholder="" maxlength="32">
		</div>
	</div>
	<div class="mt10">
		<a href="http://prestashop:1340/index.php?controller=password" class="forgot_password" rel="nofollow">{l s='Forgot your password?'}</a>
	</div>
	<div class="mt10">
		<button class="signmein btn btn-primary button_primary">{l s='Continue' d='Shop.Theme.Actions'}</button>
	</div>
</div>
