{*
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
*}
{if $display_as_info}
<div class="overlay"></div>
{/if}

{hook h='displayPaymentTop'}

{if $payment_options|count}
	<div class="payment_methods_wrapper mt10">
	{foreach from=$payment_options item="module_options"}
		{foreach from=$module_options item="option"}
		<div class="payment_option" data-action="{$option.action}">
			<div class="option_column select_icon">
				<div class="radio_icon">
					<div class="icon_body"></div>
				</div>
			</div>
			<div class="option_column payment_name">
				<span class="payment_name_wrapper">{$option.call_to_action_text}</span>
				<div class="basic_info additional_info_wrapper hidden">{$option.additionalInformation nofilter}</div>
			</div>
		</div>
		{/foreach}
	{/foreach}
	</div>

	{if $conditions_to_approve|count}
	<div id="payment_methods_conditions_to_approve" class="field_wrapper mt10">
		<div class="user_input">
			{foreach from=$conditions_to_approve item="condition" key="condition_name"}
			<div class="user_selection check" data-condition-name="{$condition_name}">
				<div class="check_icon">
					<div class="icon_body" data-name="optin"></div>
				</div>
				<div class="basic_info mb5">{$condition nofilter}</div>
			</div>
			{/foreach}
			<div class="clearfix"></div>
		</div>
	</div>
	{/if}

	{* not implemented *}
	{*
	{if $show_final_summary}
	{include file='checkout/_partials/order-final-summary.tpl'}
	{/if}
	*}

	<div id="payment_methods_confirmation" class="mt10">
		<button id="payment_methods_submit_button" type="button" disabled class="btn btn-primary center-block">
		{l s='Order with an obligation to pay' d='Shop.Theme.Checkout'}
		</button>
	</div>

	{* not implemented *}
	{*
	{if $show_final_summary}
	<article class="alert alert-danger m-t-2 js-alert-payment-conditions" role="alert" data-alert="danger">
	{l
	s='Please make sure you\'ve chosen a [1]payment method[/1] and accepted the [2]terms and conditions[/2].'
	sprintf=[
		'[1]' => '<a href="#checkout-payment-step">',
		'[/1]' => '</a>',
		'[2]' => '<a href="#conditions-to-approve">',
		'[/2]' => '</a>'
	]
	d='Shop.Theme.Checkout'
	}
	</article>
	{/if}
	*}

	{hook h='displayPaymentByBinaries'}

	<div class="modal fade" id="modal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="{l s='Close' d='Shop.Theme'}">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="js-modal-content"></div>
			</div>
		</div>
	</div>

{else}
	<p class="alert alert-danger basic_info missing_methods">{l s='Unfortunately, there are no payment method available.' d='Shop.Theme.Checkout'}</p>
{/if}
