{*
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
*}
{if $cart.vouchers.allowed}
{block name='cart_voucher'}
<div class="mt15">
	<div class="cart-voucher">
		{if $cart.vouchers.added}
			<ul id="cart_summary_voucher_list" data-token="{if isset($page_token)}{$page_token}{/if}" class="basic_info">
			{foreach from=$cart.vouchers.added item=voucher}
				<li class="cart_summary_line">
					<span class="basic_info lh24 dib">{$voucher.name}</span>
					<a href="{$voucher.delete_url}" class="removevoucher lh24 dib" data-remove-voucher="{$voucher.id_cart_rule}"><i class="material-icons dib lh24">&#xE872;</i></a>
					<div class="f_right dib lh24">
						{$voucher.reduction_formatted}
					</div>
				</li>
			{/foreach}
			</ul>
		{/if}

		<p>
			<a class="collapse-button promo-code-button" data-toggle="collapse" href="#cart_summary_promo_code" aria-expanded="false" aria-controls="cart_summary_promo_code">
			{l s='Have a promo code?' d='Shop.Theme.Checkout'}
			</a>
		</p>

		<div id="cart_summary_promo_code" class="promo-code collapse{if $cart.discounts|count > 0 || (isset($cartsummary_errors) && count($cartsummary_errors) > 0)} in{/if}" data-action="{$urls.pages.cart}" data-link-action="add-voucher">

			<input id="cart_summary_promo_code_token" type="hidden" name="token" value="{$static_token}">
			<div class="field_wrapper">
				<div class="user_input">
					<input id="cart_summary_promo_code_discountname" type="text" name="discountName" placeholder="{l s='Promo code' d='Shop.Theme.Checkout'}" maxlength="64" value="{if isset($cart_summary_promo_code_discountname)}{$cart_summary_promo_code_discountname}{/if}">
				</div>
			</div>
			<div class="mt10">
				<button id="cart_summary_promo_code_add" class="btn btn-primary button_primary" type="button"><span>{l s='Add' d='Shop.Theme.Actions'}</span></button>
			</div>

			{if isset($cartsummary_errors) && count($cartsummary_errors) > 0}
			{foreach from=$cartsummary_errors item=error}
			<div class="alert alert-danger mt10 basic_info error_box">
				<i class="material-icons">&#xE001;</i><span class="ml-1 js-error-text lh24 dib">{$error}</span>
			</div>
			{/foreach}
			{/if}
		</div>

		{if $cart.discounts|count > 0}
		<p class="basic_info mt15 mb5">
			{l s='Take advantage of our exclusive offers:' d='Shop.Theme.Actions'}
		</p>
		<ul class="basic_info">
			{foreach from=$cart.discounts item=discount}
			<li class="cart_summary_line">
				<span class="label basic_info">1NFEF23 - DISCNAME</span>
			</li>
			{/foreach}
		</ul>
		{/if}
	</div>
</div>
{/block}
{/if}
