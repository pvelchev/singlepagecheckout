<?php
/**
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
 */
class NlCondition extends ObjectModel
{
    public static $definition = array('table' => 'spciw_nl_condition', 'primary' => 'id_condition', 'multilang' => true, 'fields' => array('name' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 128, 'lang' => true, 'required' => true), 'use_amount' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true), 'amount' => array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'), 'to_amount' => array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'), 'id_currency' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'size' => 11), 'tax_incl' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true), 'use_count' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true), 'count' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'size' => 11), 'to_count' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'size' => 11), 'apply' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'values' => array('after', 'before')), 'free_shipping' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true), 'delay' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'size' => 11), 'id_cart_rule' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'size' => 11), 'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true), 'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'), 'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate')));
    public $id;
    public $name;
    public $use_amount;
    public $amount;
    public $to_amount;
    public $tax_incl;
    public $use_count;
    public $count;
    public $to_count;
    public $apply;
    public $free_shipping;
    public $id_cart_rule;
    public $show_conditions;
    public $active;
    public $date_add;
    public $date_upd;
    public $delay;
    public $gifts = array();
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);
        if ($this->id) {
            $this->getGiftProducts();
        }
    }
    private function getGiftProducts()
    {
        $gifts = DB::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'spciw_nl_condition_gift` WHERE id_condition = ' . (int) $this->id);
        foreach ($gifts as $g) {
            $this->gifts[] = (int) $g['id_product_gift'];
        }
    }
    public static function getAllConditions($apply)
    {
        $cond = DB::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'spciw_nl_condition` WHERE apply = \'' . pSQL($apply) . '\' ORDER BY amount ASC, count ASC ');
        if ($cond) {
            self::getGifts($cond);
        }
        return $cond;
    }
    public static function getGifts(&$cond)
    {
        if (is_array($cond)) {
            foreach ($cond as &$c) {
                $gifts = DB::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'spciw_nl_condition_gift` ' . 'WHERE id_condition = ' . (int) $c['id_condition']);
                if (count($cond)) {
                    $c['gifts'] = $gifts;
                }
            }
        }
    }
    public static function findApplicableCond($cart, $apply = 'before')
    {
        $sql            = new DbQuery();
        $orderTotalWt   = $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
        $orderTotal     = $cart->getOrderTotal(false, Cart::ONLY_PRODUCTS);
        $discount_total = $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS);
        $order_value_wt = $orderTotalWt - $discount_total;
        $order_value    = $orderTotal - $discount_total;
        if (Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            list($gift_total, $gift_total_wt) = self::getGiftTotalPrices($cart);
            $order_value_wt -= $gift_total_wt;
            $order_value -= $gift_total;
        }
        $product_count = self::getTotalAmountOfProducts($cart);
        $sql->select('*')->from('spciw_nl_condition')->where('apply = \'' . pSQL($apply) . '\'')->where('
                CASE WHEN use_amount = 1
                    THEN
                        CASE
                            WHEN tax_incl = 1 THEN amount < ' . (float) $order_value_wt . '  AND to_amount > ' . (float) $order_value_wt . '
                            WHEN tax_incl = 0 THEN amount < ' . (float) $order_value . '  AND to_amount > ' . (float) $order_value . '
                        END
                    END
                OR
                CASE WHEN use_count = 1
                    THEN count < ' . $product_count . '  AND to_count > ' . $product_count . '
                END
            ')->orderBy('amount DESC, count DESC')->limit(1);
        $cond = DB::getInstance()->executeS($sql);
        self::getGifts($cond);
        return $cond;
    }
    private static function getGiftTotalPrices($cart)
    {
        $total    = 0;
        $total_wt = 0;
        $products = $cart->getProductsWithSeparatedGifts();
        foreach ($products as $v) {
            if (isset($v['is_gift']) && $v['is_gift'] == true) {
                $total += $v['total'];
                $total_wt += $v['total_wt'];
            }
        }
        return array(
            $total,
            $total_wt
        );
    }
    private static function getTotalAmountOfProducts($cart)
    {
        if (Tools::version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            return $cart->nbProducts();
        }
        $products                      = $cart->getProductsWithSeparatedGifts();
        $total_amount_of_gift_products = 0;
        foreach ($products as $v) {
            if (isset($v['is_gift']) && $v['is_gift'] == true) {
                $total_amount_of_gift_products += intval($v['quantity']);
            }
        }
        return $cart->nbProducts() - $total_amount_of_gift_products;
    }
    public static function findNextApplicableCond($cart, $apply = 'before')
    {
        $orderTotalWt   = $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
        $orderTotal     = $cart->getOrderTotal(false, Cart::ONLY_PRODUCTS);
        $discount_total = $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS);
        $order_value_wt = $orderTotalWt - $discount_total;
        $order_value    = $orderTotal - $discount_total;
        $sql            = new DbQuery();
        $sql->select('*')->from('spciw_nl_condition')->where('apply = \'' . pSQL($apply) . '\'')->where(' CASE
                        WHEN use_amount = 1 THEN
                                CASE
                                    WHEN tax_incl = 1 THEN amount > ' . (float) $order_value_wt . '
                                    WHEN tax_incl = 0 THEN amount > ' . (float) $order_value . '
                                END
                        END
                    OR
                    CASE
                        WHEN use_count = 1 THEN count > ' . self::getTotalAmountOfProducts($cart) . '
                    END')->orderBy('amount ASC, count ASC')->limit(1);
        $cond = DB::getInstance()->executeS($sql);
        self::getGifts($cond);
        return $cond;
    }
    public static function attachRuleCartCondition($id_rule, $id_cart, $id_condition)
    {
        DB::getInstance()->insert('spciw_nl_condition_rule', array(
            'id_cart_rule' => (int) $id_rule,
            'id_cart' => (int) $id_cart,
            'id_condition' => (int) $id_condition
        ));
    }
    public static function detachRuleCartCondition($id_rule, $id_cart)
    {
        DB::getInstance()->delete('spciw_nl_condition_rule', 'id_cart = ' . (int) $id_cart . ' AND id_cart_rule  = ' . (int) $id_rule);
    }
    public static function getAttachedRuleCartCondition($id_cart, $id_cart_rule = null)
    {
        $sql = new DbQuery();
        $sql->select('*')->from('spciw_nl_condition_rule')->where('id_cart = ' . (int) $id_cart);
        if (!is_null($id_cart_rule)) {
            $sql->where('id_cart_rule = ' . (int) $id_cart_rule);
        }
        return DB::getInstance()->getRow($sql);
    }
    public static function issueNextPurchaseVoucher($data)
    {
        DB::getInstance()->insert('spciw_nl_issued_vouchers', $data, false, true, DB::INSERT_IGNORE);
    }
    public static function hasIssuedVoucher($id_order)
    {
        return DB::getInstance()->getRow('SELECT * FROM `' . _DB_PREFIX_ . 'spciw_nl_issued_vouchers` WHERE id_order = ' . (int) $id_order);
    }
    public static function getDelayedVouchers()
    {
        $sql = new DbQuery();
        $sql->select('*')->from('spciw_nl_issued_vouchers')->where('delay = 1')->where('sent = 0')->where('delay_date < NOW()')->orderBy('delay_date ASC')->limit(2);
        return DB::getInstance()->executeS($sql);
    }
    public function add($autodate = true, $null_values = false)
    {
        $ret = parent::add($autodate, $null_values);
        if ($ret) {
            $this->addGiftProducts();
        }
        return $ret;
    }
    private function addGiftProducts()
    {
        foreach ($this->gifts as $g) {
            DB::getInstance()->insert('spciw_nl_condition_gift', array(
                'id_condition' => (int) $this->id,
                'id_product_gift' => (int) $g
            ));
        }
    }
    public function update($null_values = false)
    {
        $ret = parent::update($null_values);
        if ($ret) {
            $this->deleteGiftProducts();
            $this->addGiftProducts();
        }
        return $ret;
    }
    private function deleteGiftProducts()
    {
        DB::getInstance()->delete('spciw_nl_condition_gift', 'id_condition = ' . (int) $this->id);
    }
    public function delete()
    {
        $ret = parent::delete();
        if ($ret) {
            $this->deleteGiftProducts();
        }
        return $ret;
    }
}