CREATE TABLE IF NOT EXISTS `PREFIX_spciw_nl_condition` (
	`id_condition` int(11) NOT NULL AUTO_INCREMENT ,
	`use_amount` tinyint(1) NOT NULL,
	`amount` decimal(10,2) NOT NULL,
	`to_amount` DECIMAL(10,2) NOT NULL,
	`id_currency` int(11) NOT NULL,
	`convert` tinyint(1) NOT NULL,
	`tax_incl` tinyint(1) NOT NULL,
	`shipping_incl` tinyint(1) NOT NULL,
	`use_count` tinyint(1) NOT NULL,
	`count` int(11) NOT NULL,
	`to_count` INT(11) NOT NULL,
	`apply` varchar(64) NOT NULL,
	`free_shipping` tinyint(1) NOT NULL,
	`id_product_gift` int(11) NOT NULL,
	`id_cart_rule` INT(11) NOT NULL,
	`delay` INT(11) NOT NULL,
	`show_condtitions` tinyint(1) NOT NULL,
	`active` tinyint(1) NOT NULL,
	`date_add` datetime NOT NULL,
	`date_upd` datetime NOT NULL,
	PRIMARY KEY (`id_condition`)
) ENGINE=_ENGINE_ DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_spciw_nl_condition_gift` (
	`id_condition` int(11) NOT NULL,
	`id_product_gift` int(11) NOT NULL,
	`id_product_attribute` int(11) NOT NULL,
	PRIMARY KEY (`id_condition`,`id_product_gift`)
) ENGINE=_ENGINE_ DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_spciw_nl_condition_lang` (
	`id_condition` int(11) NOT NULL,
	`id_lang` int(11) NOT NULL,
	`name` varchar(128) NOT NULL,
	PRIMARY KEY (`id_condition`,`id_lang`)
) ENGINE=_ENGINE_ DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_spciw_nl_condition_rule` (
	`id_cart_rule` int(11) NOT NULL,
	`id_cart` int(11) NOT NULL,
	`id_condition` int(11) NOT NULL
) ENGINE=_ENGINE_ DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_spciw_nl_issued_vouchers` (
	`id_order` int(11) NOT NULL,
	`id_cart_rule` int(11) NOT NULL,
	`delay` TINYINT(1) NOT NULL,
	`id_condition` INT(11) NOT NULL,
	`delay_date` DATETIME NOT NULL,
	`sent` TINYINT(1) NOT NULL,
	PRIMARY KEY( `id_order`, `id_cart_rule`)
) ENGINE=_ENGINE_ DEFAULT CHARSET=utf8;
