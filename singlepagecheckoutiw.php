<?php
/**
 * Prestashop Modules & Themen End User License Agreement
 *
 * This End User License Agreement ("EULA") is a legal agreement between you and Presta-Apps ltd.
 * ( here in referred to as "we" or "us" ) with regard to Prestashop Modules & Themes
 * (herein referred to as "Software Product" or "Software").
 * By installing or using the Software Product you agree to be bound by the terms of this EULA.
 *
 * 1. Eligible Licensees. This Software is available for license solely to Software Owners,
 * with no right of duplication or further distribution, licensing, or sub-licensing.
 * A Software Owner is someone who legally obtained a copy of the Software Product via Prestashop Store.
 *
 * 2. License Grant. We grant you a personal/one commercial, non-transferable and non-exclusive right to use the copy
 * of the Software obtained via Prestashop Store. Modifying, translating, renting, copying, transferring or assigning
 * all or part of the Software, or any rights granted hereunder, to any other persons and removing any proprietary
 * notices, labels or marks from the Software is strictly prohibited. Furthermore, you hereby agree not to create
 * derivative works based on the Software. You may not transfer this Software.
 *
 * 3. Copyright. The Software is licensed, not sold. You acknowledge that no title to the intellectual property in the
 * Software is transferred to you. You further acknowledge that title and full ownership rights to the Software will
 * remain the exclusive property of Presta-Apps Mobile, and you will not acquire any rights to the Software,
 * except as expressly set forth above.
 *
 * 4. Reverse Engineering. You agree that you will not attempt, and if you are a corporation,
 * you will use your best efforts to prevent your employees and contractors from attempting to reverse compile, modify,
 * translate or disassemble the Software in whole or in part. Any failure to comply with the above or any other terms
 * and conditions contained herein will result in the automatic termination of this license.
 *
 * 5. Disclaimer of Warranty. The Software is provided "AS IS" without warranty of any kind. We disclaim and make no
 * express or implied warranties and specifically disclaim the warranties of merchantability, fitness for a particular
 * purpose and non-infringement of third-party rights. The entire risk as to the quality and performance of the Software
 * is with you. We do not warrant that the functions contained in the Software will meet your requirements or that the
 * operation of the Software will be error-free.
 *
 * 6. Limitation of Liability. Our entire liability and your exclusive remedy under this EULA shall not exceed the price
 * paid for the Software, if any. In no event shall we be liable to you for any consequential, special, incidental or
 * indirect damages of any kind arising out of the use or inability to use the software.
 *
 * 7. Rental. You may not loan, rent, or lease the Software.
 *
 * 8. Updates and Upgrades. All updates and upgrades of the Software from a previously released version are governed by
 * the terms and conditions of this EULA.
 *
 * 9. Support. Support for the Software Product is provided by Presta-Apps ltd. For product support, please send an
 * email to support at info@iniweb.de
 *
 * 10. No Liability for Consequential Damages. In no event shall we be liable for any damages whatsoever
 * (including, without limitation, incidental, direct, indirect special and consequential damages, damages for loss
 * of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of
 * the use or inability to use the Software Product. Because some states/countries do not allow the exclusion or
 * limitation of liability for consequential or incidental damages, the above limitation may not apply to you.
 *
 * 11. Indemnification by You. You agree to indemnify, hold harmless and defend us from and against any claims or
 * lawsuits, including attorney's fees that arise or result from the use or distribution of the Software in violation
 * of this Agreement.
 *
 * @author    Presta-Apps Limited
 * @website   www.presta-apps.com
 * @contact   info@presta-apps.com
 * @copyright 2009-2016 Presta-Apps Ltd.
 * @license   Proprietary
 */
require __DIR__ . '/inixframe/loader.php';
class Singlepagecheckoutiw extends Inix2Module
{
    const COND_NONE = 0;
    const COND_NEXT = 1;
    const COND_ALL = 2;
    public static $action_cart = false;
    private $executed_hook = false;
    private $show_conditions;
    private $nl_enabled = false;
    private $color_scheme;
    private $layoutNames = array(1 => 'Classic', 2 => 'Pizza', 3 => 'Piperony', 4 => 'Slice', 5 => 'Domino');
    private $color_scheme_items_prefix = 'spciw_color_scheme_';
    private $color_scheme_names = array();
    public function __construct()
    {
        $this->name                   = 'singlepagecheckoutiw';
        $this->tab                    = 'checkout';
        $this->version                = '1.4.0';
        $this->author                 = 'Presta-Apps';
        $this->need_instance          = 0;
        $this->ps_versions_compliancy = array(
            'min' => '1.7.0',
            'max' => '1.7.9'
        );
        $this->bootstrap              = true;
        parent::__construct();
        $this->displayName      = $this->l('Single Page Checkout');
        $this->description      = $this->l('Replace the standard page of checkout with single page checkout.');
        $this->confirmUninstall = $this->l('Are you sure you want to unistall Single Page Checkout?');
        $this->show_conditions  = Configuration::get('spciw_nl_cart_cond');
        $this->getNLEnabledState();
    }
    public function install()
    {
        $this->setupDefaultThemeColors();
        Configuration::updateValue('spciw_select_page_layout', 1);
        Configuration::updateValue('spciw_hide_last_3_checkout_steps', 0);
        Configuration::updateValue('spciw_display_the_content_of_last_2_steps_as_info', 1);
        Configuration::updateValue('spciw_display_reassurance_block', 1);
        Configuration::updateValue('spciw_display_progress_bar', 1);
        Configuration::updateValue('spciw_display_optional_form_field_company', 1);
        Configuration::updateValue('spciw_display_optional_form_field_vat_number', 1);
        Configuration::updateValue('spciw_display_optional_form_field_address_complement', 1);
        Configuration::updateValue('spciw_display_optional_form_field_phone', 1);
        Configuration::updateValue('spciw_display_optional_form_field_birthday', 1);
        Configuration::updateValue('spciw_nl_enabled', 0);
        Configuration::updateValue('spciw_use_the_default_color_scheme', 1);
        $this->install_hooks = array(
            'displayAfterBodyOpeningTag',
            'displayShoppingCartFooter',
            'actionCartSave',
            'actionObjectOrderAddAfter',
            'displayHeader',
            'displayOrderConfirmation'
        );
        $this->installMails();
        return parent::install();
    }
    public function uninstall()
    {
        Configuration::deleteByName('spciw_select_page_layout');
        Configuration::deleteByName('spciw_hide_last_3_checkout_steps');
        Configuration::deleteByName('spciw_display_the_content_of_last_2_steps_as_info');
        Configuration::deleteByName('spciw_display_reassurance_block');
        Configuration::deleteByName('spciw_display_progress_bar');
        Configuration::deleteByName('spciw_display_optional_form_field_company');
        Configuration::deleteByName('spciw_display_optional_form_field_vat_number');
        Configuration::deleteByName('spciw_display_optional_form_field_address_complement');
        Configuration::deleteByName('spciw_display_optional_form_field_phone');
        Configuration::deleteByName('spciw_display_optional_form_field_birthday');
        Configuration::deleteByName('spciw_nl_enabled');
        Configuration::deleteByName('spciw_use_the_default_color_scheme');
        return parent::uninstall();
    }
    private function setupDefaultThemeColors()
    {
        require_once($this->local_path . 'classes/ThemeColors.php');
        $source = ThemeColors::getDefaultSource();
        foreach ($source as $name => $color) {
            Configuration::updateValue($this->color_scheme_items_prefix . $name, $color);
        }
    }
    private function initThemeColors()
    {
        require_once($this->local_path . 'classes/ThemeColors.php');
        return ThemeColors::getDefaultSource();
    }
    public function installMails($from_layout = 1)
    {
        $install_mails = array(
            'new_voucher'
        );
        $langs         = Language::getLanguages(false);
        $mails         = true;
        foreach ($install_mails as $m) {
            foreach ($langs as $l) {
                if (!is_dir(_PS_MODULE_DIR_ . $this->name . '/mails/' . $l['iso_code'])) {
                    $mails &= @mkdir(_PS_MODULE_DIR_ . $this->name . '/mails/' . $l['iso_code'], 0777, true);
                }
                if (file_exists(_PS_MODULE_DIR_ . $this->name . '/mails/' . $l['iso_code'] . '/' . $m . '.html')) {
                    unlink(_PS_MODULE_DIR_ . $this->name . '/mails/' . $l['iso_code'] . '/' . $m . '.html');
                }
                if (file_exists(_PS_MODULE_DIR_ . $this->name . '/mails/' . $l['iso_code'] . '/' . $m . '.txt')) {
                    unlink(_PS_MODULE_DIR_ . $this->name . '/mails/' . $l['iso_code'] . '/' . $m . '.txt');
                }
                $mails &= @copy(_PS_MODULE_DIR_ . $this->name . '/mail_tpl/' . $from_layout . '/' . $m . '.html', _PS_MODULE_DIR_ . $this->name . '/mails/' . $l['iso_code'] . '/' . $m . '.html');
                $mails &= @copy(_PS_MODULE_DIR_ . $this->name . '/mail_tpl/' . $from_layout . '/' . $m . '.txt', _PS_MODULE_DIR_ . $this->name . '/mails/' . $l['iso_code'] . '/' . $m . '.txt');
            }
        }
    }
    public function formatApply($v)
    {
        if ($v == 'before') {
            return $this->l('Before purchase');
        } else {
            return $this->l('After purchase');
        }
    }
    public function printBoolIcon($v)
    {
        $tpl = $this->createTemplate('bool_button.tpl');
        $tpl->assign('value', $v);
        return $tpl->fetch();
    }
    public function printAmountBoolIcon($v, $tr)
    {
        $tpl = $this->createTemplate('bool_button.tpl');
        $tpl->assign('value', $v);
        if ($v) {
            $currency = Currency::getCurrency($tr['id_currency']);
            if ($currency == false) {
                $currency = null;
            }
            $tpl->assign('amount', array(
                'from' => Tools::displayPrice($tr['amount'], $currency),
                'to' => Tools::displayPrice($tr['to_amount'], $currency)
            ));
        }
        return $tpl->fetch();
    }
    public function printCountBoolIcon($v, $tr)
    {
        $tpl = $this->createTemplate('bool_button.tpl');
        $tpl->assign('value', $v);
        if ($v) {
            $tpl->assign('amount', array(
                'from' => $tr['count'],
                'to' => $tr['to_count']
            ));
        }
        return $tpl->fetch();
    }
    public function renderForm()
    {
        $this->fields_form = array(
            'legend' => array(
                'title' => Validate::isLoadedObject($this->object) ? $this->l('Edit condition') : $this->l('Create condition')
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    'lang' => true,
                    'size' => 48,
                    'required' => true,
                    'desc' => $this->l('Only for recognition in back office')
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Depends on cart amount'),
                    'required' => true,
                    'name' => 'use_amount',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'use_amount_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'use_amount_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('More than'),
                    'name' => 'amount',
                    'size' => 48,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Less than'),
                    'name' => 'to_amount',
                    'size' => 48,
                    'required' => true
                ),
                array(
                    'label' => $this->l('Currency'),
                    'name' => 'id_currency',
                    'type' => 'select',
                    'required' => true,
                    'options' => array(
                        'query' => Currency::getCurrencies(),
                        'id' => 'id_currency',
                        'name' => 'sign'
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Tax included'),
                    'required' => true,
                    'name' => 'tax_incl',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'tax_incl_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'tax_incl_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Shipping included'),
                    'required' => true,
                    'name' => 'shipping_incl',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'shipping_incl_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'shipping_incl_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Depends on products count'),
                    'required' => true,
                    'name' => 'use_count',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => '_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => '_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Count more than'),
                    'name' => 'count',
                    'size' => 48,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Count less than'),
                    'name' => 'to_count',
                    'size' => 48,
                    'required' => true
                ),
                array(
                    'label' => $this->l('When to apply condition'),
                    'name' => 'apply',
                    'type' => 'select',
                    'required' => true,
                    'options' => array(
                        'query' => array(
                            array(
                                'name' => $this->l('After purchase'),
                                'id' => 'after'
                            ),
                            array(
                                'name' => $this->l('Before purchase'),
                                'id' => 'before'
                            )
                        ),
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Active'),
                    'required' => true,
                    'name' => 'active',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                    'default_value' => 1
                ),
                array(
                    'type' => 'heading',
                    'text' => $this->l('Condition will apply'),
                    'name' => 's'
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Free shipping'),
                    'required' => true,
                    'name' => 'free_shipping',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'free_shipping_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'free_shipping_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    )
                ),
                array(
                    'label' => $this->l('Gift product'),
                    'type' => 'products',
                    'selected_products' => $this->object->gifts,
                    'name' => 'gifts'
                ),
                array(
                    'label' => $this->l('Sample Voucher'),
                    'name' => 'id_cart_rule',
                    'type' => 'select',
                    'options' => array(
                        'default' => array(
                            'value' => '',
                            'label' => '--'
                        ),
                        'query' => CartRule::getCartsRuleByCode('', $this->context->language->id),
                        'id' => 'id_cart_rule',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Select a sample voucher, which values will be used to ' . 'generate a new voucher for the paricular client ')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Delay'),
                    'name' => 'delay',
                    'size' => 48,
                    'default_value' => 0,
                    'desc' => $this->l('Delay issuing a cart rule by a given days. ' . 'Set to 0 to issue cart rule immediately after purchase')
                )
            ),
            'submit' => array(
                'title' => $this->l('Save')
            )
        );
        return parent::renderForm();
    }
    private function renderConditions($cart)
    {
        if (!$this->show_conditions) {
            return '';
        }
        $cond                   = array();
        $products               = $cart->getProducts();
        $total_products         = $cart->getOrderTotal(false, Cart::ONLY_PRODUCTS);
        $total_products_wt      = $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
        $total_shipping         = $cart->getOrderTotal(true, Cart::ONLY_SHIPPING);
        $total_shipping_tax_exc = $cart->getOrderTotal(false, Cart::ONLY_SHIPPING);
        if ($this->show_conditions == self::COND_ALL) {
            $cond = NlCondition::getAllConditions('before');
        } elseif ($this->show_conditions == self::COND_NEXT) {
            $cond = NlCondition::findNextApplicableCond($cart);
        }
        foreach ($cond as &$c) {
            if (isset($c['gifts'])) {
                foreach ($c['gifts'] as &$g) {
                    $product            = new Product($g['id_product_gift'], false, $this->context->language->id);
                    $g['product_name']  = $product->name;
                    $image              = Image::getCover($product->id);
                    $g['product_image'] = array(
                        $product->link_rewrite,
                        $product->id . '-' . $image['id_image'],
                        (method_exists('ImageType', 'getFormatedName') ? ImageType::getFormatedName('medium') : 'medium' . '_' . 'default')
                    );
                }
            }
        }
        $this->context->smarty->assign(array(
            'conditions' => $cond,
            'nl_products' => $products,
            'nl_total_products' => $total_products,
            'nl_total_products_wt' => $total_products_wt,
            'nl_total_shipping' => $total_shipping,
            'nl_total_shipping_tax_exc' => $total_shipping_tax_exc
        ));
        return $this->display(__FILE__, 'cart_summary_ps17.tpl');
    }
    public function hookDisplayShoppingCartFooter($params)
    {
        if ($this->getNLEnabledState() == 0) {
            return;
        }
        if (!$this->executed_hook) {
            $this->executed_hook = true;
            return $this->renderConditions($params['cart']);
        }
    }
    public function hookActionObjectOrderAddAfter($params)
    {
        if ($this->getNLEnabledState() == 0) {
            return;
        }
        $order   = $params['object'];
        $id_cart = $order->id_cart;
        $cart    = new Cart($id_cart);
        $rules   = $cart->getCartRules();
        foreach ($rules as $r) {
            NlCondition::detachRuleCartCondition($r['obj']->id, $cart->id);
        }
    }
    public function hookActionCartSave($params)
    {
        if ($this->getNLEnabledState() == 0) {
            return;
        }
        $cart = $this->context->cart;
        if (!Validate::isLoadedObject($cart)) {
            return;
        }
        if (self::$action_cart) {
            return;
        }
        self::$action_cart = true;
        $rule              = NlCondition::getAttachedRuleCartCondition($cart->id);
        $applicableCond    = NlCondition::findApplicableCond($cart);
        if (!$applicableCond) {
            if ($rule != false) {
                $cart->removeCartRule($rule['id_cart_rule']);
                NlCondition::detachRuleCartCondition($rule['id_cart_rule'], $cart->id);
                $cart_rule = new CartRule($rule['id_cart_rule']);
                $cart_rule->delete();
            }
        } else {
            if (!$rule) {
                $cart_rule                                              = new CartRule();
                $cart_rule->name[Configuration::get('PS_LANG_DEFAULT')] = $this->l('Next level voucher');
                $cart_rule->code                                        = $this->generateRandomString();
                $cart_rule->partial_use                                 = false;
                $cart_rule->active                                      = 1;
                if (Validate::isLoadedObject($this->context->customer)) {
                    $cart_rule->id_customer = $this->context->customer->id;
                }
                $cart_rule->quantity          = 1;
                $cart_rule->quantity_per_user = 1;
                $cart_rule->date_from         = date('Y-m-d H:i:s', time());
                $cart_rule->date_to           = date('Y-m-d H:i:s', time() + (30 * 24 * 60 * 60));
                if ($applicableCond[0]['free_shipping']) {
                    $cart_rule->free_shipping = true;
                }
                if ($applicableCond[0]['gifts']) {
                    $cart_rule->gift_product = $applicableCond[0]['gifts'][0]['id_product_gift'];
                }
                if ($cart_rule->save()) {
                    $cart->addCartRule($cart_rule->id);
                    NlCondition::attachRuleCartCondition($cart_rule->id, $cart->id, $applicableCond[0]['id_condition']);
                }
            } else {
                if ($rule['id_condition'] != $applicableCond[0]['id_condition']) {
                    $cart_rule = new CartRule($rule['id_cart_rule']);
                    if ($applicableCond[0]['free_shipping']) {
                        $cart_rule->free_shipping = true;
                    }
                    if ($applicableCond[0]['gifts']) {
                        $cart_rule->gift_product = $applicableCond[0]['gifts'][0]['id_product_gift'];
                    } else {
                        $cart->updateQty(1, $cart_rule->gift_product, null, null, 'down', 0, null, false);
                        $cart_rule->gift_product = 0;
                    }
                    if (Validate::isLoadedObject($cart_rule) && $cart_rule->save()) {
                        $cart->removeCartRule($cart_rule->id);
                        $cart->addCartRule($cart_rule->id);
                        NlCondition::detachRuleCartCondition($rule['id_cart_rule'], $cart->id);
                        NlCondition::attachRuleCartCondition($cart_rule->id, $cart->id, $applicableCond[0]['id_condition']);
                    }
                }
            }
        }
    }
    public function hookDisplayOrderConfirmation($params)
    {
        if ($this->getNLEnabledState() == 0) {
            return;
        }
        $order          = (Tools::version_compare(_PS_VERSION_, '1.7.0.0', '<')) ? $params['objOrder'] : $params['order'];
        $cart           = new Cart($order->id_cart);
        $customer       = new Customer($order->id_customer);
        $issued_voucher = NlCondition::hasIssuedVoucher($order->id);
        if (!$issued_voucher) {
            $id_cart_rule = false;
            $send_email   = true;
        } else {
            $id_cart_rule = $issued_voucher['id_cart_rule'];
            $delayed      = (bool) $issued_voucher['delay'];
            $send_email   = false;
        }
        if ($id_cart_rule) {
            $cart_rule = new CartRule($id_cart_rule);
        } else {
            $applicableCond = NlCondition::findApplicableCond($cart, 'after');
            if ($applicableCond) {
                $cond    = $applicableCond[0];
                $delayed = (bool) $cond['delay'];
                if ($cond['id_cart_rule']) {
                    $cart_rule = new CartRule($cond['id_cart_rule']);
                } else {
                    $cart_rule = new CartRule();
                }
                $cart_rule->id          = null;
                $cart_rule->name        = array(
                    Configuration::get('PS_LANG_DEFAULT') => $this->l('Next purchase voucher')
                );
                $cart_rule->code        = $this->generateRandomString();
                $cart_rule->partial_use = false;
                if (!$delayed) {
                    $cart_rule->active = 1;
                } else {
                    $cart_rule->active = 0;
                }
                $cart_rule->id_customer       = $order->id_customer;
                $cart_rule->quantity          = 1;
                $cart_rule->quantity_per_user = 1;
                if (!$delayed) {
                    $cart_rule->date_from = date('Y-m-d H:i:s', strtotime("now"));
                    $cart_rule->date_to   = date('Y-m-d H:i:s', strtotime("+1 month"));
                } else {
                    $cart_rule->date_from = date('Y-m-d H:i:s', strtotime("+" . (int) $cond['delay'] . " days"));
                    $cart_rule->date_to   = date('Y-m-d H:i:s', strtotime("+" . (int) $cond['delay'] . " days +1 month"));
                }
                if (!$cond['id_cart_rule']) {
                    if ($cond['free_shipping']) {
                        $cart_rule->free_shipping = true;
                    }
                    if ($cond['gifts']) {
                        $cart_rule->gift_product = $cond['gifts'][0]['id_product_gift'];
                    }
                }
                if ($cart_rule->save()) {
                    NlCondition::issueNextPurchaseVoucher(array(
                        'id_cart_rule' => (int) $cart_rule->id,
                        'id_order' => (int) $order->id,
                        'id_condition' => (int) $cond['id_condition'],
                        'delay' => $delayed,
                        'delay_date' => pSQL($cart_rule->date_from)
                    ));
                }
            }
        }
        if (!isset($cart_rule, $delayed)) {
            $content = false;
        } else {
            $content = $this->prepareCongratsContent($cart_rule, $delayed);
        }
        if ($content !== false && $send_email) {
            $this->sendCongratsEmail($content, $customer);
            DB::getInstance()->update('nl_issued_vouchers', array(
                'sent' => 1
            ), 'id_cart_rule = ' . (int) $cart_rule->id . ' AND id_order = ' . (int) $order->id);
        }
        return $content;
    }
    public function generateRandomString($length = 10)
    {
        return Tools::substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
    public function prepareCongratsContent($cart_rule, $delayed)
    {
        $this->context->smarty->assign('nl_cart_rule', $cart_rule);
        if (Validate::isLoadedObject($cart_rule)) {
            if (!$delayed) {
                if ($cart_rule->gift_product) {
                    $prod       = new Product($cart_rule->gift_product, false, $this->context->language->id);
                    $image      = Image::getCover($prod->id);
                    $prod_image = array(
                        $prod->link_rewrite,
                        $prod->id . '-' . $image['id_image'],
                        (method_exists('ImageType', 'getFormatedName') ? ImageType::getFormatedName('medium') : 'medium' . '_' . 'default')
                    );
                    $this->context->smarty->assign('nl_product', array(
                        'product_name' => $prod->name,
                        'product_image' => $prod_image
                    ));
                }
                $content = $this->display(__FILE__, 'order_confirm.tpl');
                return $content;
            }
        }
        return false;
    }
    public function hookDisplayHeader()
    { 
        $this->context->controller->addCSS($this->_path . 'views/css/front.css');
        $this->context->controller->addJS($this->_path . 'views/js/front.js');
        if ($this->getNLEnabledState() == 0) {
            return;
        }
        $for_sending = NlCondition::getDelayedVouchers();
        if ($for_sending) {
            foreach ($for_sending as $issued) {
                $cart_rule = new CartRule($issued['id_cart_rule']);
                $customer  = new Customer($cart_rule->id_customer);
                $content   = $this->prepareCongratsContent($cart_rule, false);
                if ($content !== false) {
                    if ($this->sendCongratsEmail($content, $customer)) {
                        $cart_rule->active = 1;
                        $cart_rule->save();
                        DB::getInstance()->update('nl_issued_vouchers', array(
                            'sent' => 1
                        ), 'id_cart_rule = ' . (int) $issued['id_cart_rule'] . ' AND id_order = ' . (int) $issued['id_order']);
                    }
                }
            }
        }
    }
    public function hookDisplayAfterBodyOpeningTag()
    {
        $this->context->smarty->assign(array(
            'redirect_url' => $this->context->link->getModuleLink($this->name, 'checkout')
        ));
        return $this->context->smarty->fetch('module:singlepagecheckoutiw/views/templates/hook/redirect_url.tpl');
    }
    public function getContent()
    {
        $fields                                       = array();
        $fields['spciw_select_page_layout']           = array(
            'title' => $this->l('Select Page Layout'),
            'desc' => 'This setting is allowing you to select a layout for your Checkout page',
            'cast' => 'intval',
            'validation' => 'isInt',
            'type' => 'select',
            'default' => 1,
            'required' => false,
            'list' => array(
                array(
                    'name' => $this->l($this->layoutNames[1]),
                    'value' => 1
                ),
                array(
                    'name' => $this->l($this->layoutNames[2]),
                    'value' => 2
                ),
                array(
                    'name' => $this->l($this->layoutNames[3]),
                    'value' => 3
                ),
                array(
                    'name' => $this->l($this->layoutNames[4]),
                    'value' => 4
                ),
                array(
                    'name' => $this->l($this->layoutNames[5]),
                    'value' => 5
                )
            ),
            'identifier' => 'value'
        );
        $fields['spciw_use_the_default_color_scheme'] = array(
            'title' => $this->l('Use the default color scheme'),
            'desc' => $this->l(''),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 1,
            'required' => false
        );
        $scheme_color_name                            = $this->initThemeColors();
        foreach ($scheme_color_name as $color_name => $color_value) {
            $display_name                                           = ucfirst(strtolower(str_replace('_', ' ', $color_name)));
            $fields[$this->color_scheme_items_prefix . $color_name] = array(
                'title' => $this->l($display_name),
                'desc' => '',
                'validation' => 'isString',
                'cast' => 'strval',
                'type' => 'color',
                'auto_value' => true
            );
        }
        $fields['spciw_hide_last_3_checkout_steps']                     = array(
            'title' => $this->l('Hide boxes that cannot be changed at the time'),
            'desc' => $this->l(''),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 0,
            'required' => false
        );
        $fields['spciw_display_the_content_of_last_2_steps_as_info']    = array(
            'title' => $this->l('Display the content of last the 2 steps'),
            'desc' => $this->l('The content of Shipping and Payment methods will be displayed as non-editable until Address step is completed'),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 1,
            'required' => false
        );
        $fields['spciw_display_reassurance_block']                      = array(
            'title' => $this->l('Display reassurance section'),
            'desc' => $this->l(''),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 1,
            'required' => false
        );
        $fields['spciw_display_optional_form_field_company']            = array(
            'title' => $this->l('Display optional form field: Company'),
            'desc' => $this->l(''),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 1,
            'required' => false
        );
        $fields['spciw_display_optional_form_field_vat_number']         = array(
            'title' => $this->l('Display optional form field: VAT Number'),
            'desc' => $this->l(''),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 1,
            'required' => false
        );
        $fields['spciw_display_optional_form_field_address_complement'] = array(
            'title' => $this->l('Display optional form field: Address Complement'),
            'desc' => $this->l(''),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 1,
            'required' => false
        );
        $fields['spciw_display_optional_form_field_phone']              = array(
            'title' => $this->l('Display optional form field: Phone'),
            'desc' => $this->l(''),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 1,
            'required' => false
        );
        $fields['spciw_display_optional_form_field_birthday']           = array(
            'title' => $this->l('Display optional form field: Birthday'),
            'desc' => $this->l(''),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 1,
            'required' => false
        );
        $fields['spciw_display_progress_bar']                           = array(
            'title' => $this->l('Display progress bar'),
            'desc' => $this->l(''),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 1,
            'required' => false
        );
        $fields['spciw_nl_enabled']                                     = array(
            'title' => $this->l('Enable integrated Next Level Voucher module'),
            'desc' => $this->l(''),
            'validation' => 'isBool',
            'cast' => 'intval',
            'type' => 'bool',
            'default' => 0,
            'required' => false
        );
        $fields['spciw_nl_cart_cond']                                   = array(
            'title' => $this->l('Next Level Voucher: Show conditions on cart page'),
            'type' => 'select',
            'list' => array(
                array(
                    'value' => self::COND_NONE,
                    'name' => $this->l('Do not show conditions')
                ),
                array(
                    'value' => self::COND_NEXT,
                    'name' => $this->l('Show only next achievable condition')
                ),
                array(
                    'value' => self::COND_ALL,
                    'name' => $this->l('Show all conditions')
                )
            ),
            'identifier' => 'value'
        );
        $fields['spciw_nl_mail_layout']                                 = array(
            'title' => $this->l('Next Level Voucher: Email layout'),
            'desc' => '',
            'validation' => 'isInt',
            'cast' => 'intval',
            'type' => 'select',
            'list' => array(
                array(
                    'name' => 'Default',
                    'value' => 1
                ),
                array(
                    'name' => 'Stylish ',
                    'value' => 2
                ),
                array(
                    'name' => 'Lite',
                    'value' => 3
                )
            ),
            'identifier' => 'value'
        );
        $this->fields_options                                           = array(
            'general' => array(
                'title' => $this->l('General settings'),
                'icon' => '',
                'description' => '',
                'info' => '',
                'fields' => $fields,
                'submit' => array(
                    'title' => $this->l('Save')
                )
            )
        );
        $this->className                                                = 'NlCondition';
        $this->object_table                                             = 'spciw_nl_condition';
        $this->object_identifier                                        = 'id_condition';
        $this->lang                                                     = true;
        $this->show_toolbar                                             = false;
        $this->fields_list                                              = array(
            'id_condition' => array(
                'width' => 10,
                'title' => 'ID'
            ),
            'name' => array(
                'title' => $this->l('Name')
            ),
            'use_amount' => array(
                'title' => $this->l('Depends on amount'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printAmountBoolIcon',
                'orderby' => false,
                'width' => 10
            ),
            'use_count' => array(
                'title' => $this->l('Depends on products count'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printCountBoolIcon',
                'orderby' => false,
                'width' => 10
            ),
            'tax_incl' => array(
                'title' => $this->l('Tax including'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printBoolIcon',
                'orderby' => false,
                'width' => 10
            ),
            'apply' => array(
                'title' => $this->l('Applying'),
                'callback' => 'formatApply',
                'orderby' => false,
                'filter_key' => 'a!apply',
                'type' => 'select',
                'list' => array(
                    'before' => $this->l('Before purchase'),
                    'after' => $this->l('After purchase')
                ),
                'width' => 150
            ),
            'active' => array(
                'title' => $this->l('Enabled'),
                'align' => 'text-center',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false,
                'filter_key' => 'a!active',
                'width' => 10
            )
        );
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        if ((int) Configuration::get('PS_DISABLE_OVERRIDES') == 1) {
            $this->errors[] = $this->l('Prestashop override feature is currently disabled! Next Level Voucher requires this feature enabled to work properly.');
        }
        return parent::getContent();
    }
    public function beforeUpdateOptions()
    {
    }
    public function sendCongratsEmail($content, $customer)
    {
        $vars = array(
            '{content}' => $content
        );
        $this->additionalMailFields($vars);
        return Mail::Send($customer->id_lang, 'new_voucher', $this->l('Congratulations'), $vars, $customer->email, $customer->firstname . ' ' . $customer->lastname, null, null, null, null, $this->getLocalPath() . 'mails/');
    }
    public function updateOptionNlMailLayout($val)
    {
        $old_style = Configuration::get('spciw_nl_mail_layout');
        if ($val == $old_style) {
            return;
        }
        if (in_array((int) $val, array(
            1,
            2,
            3
        ))) {
            $this->installMails((int) $val);
            Configuration::updateValue('spciw_nl_mail_layout', (int) $val);
        }
    }
    public function displayAjaxpreviewlayout()
    {
        $layout = (int) Tools::getValue('layout');
        if (!in_array($layout, array(
            1,
            2,
            3
        ))) {
            $this->errors[] = $this->l('Invalid layout selected!');
        } else {
            $vars = array();
            if (Configuration::get('PS_LOGO_MAIL') !== false && file_exists(_PS_IMG_DIR_ . Configuration::get('PS_LOGO_MAIL', null, null, $this->context->shop->id))) {
                $logo = _PS_IMG_ . Configuration::get('PS_LOGO_MAIL', null, null, $this->context->shop->id);
            } else {
                if (file_exists(_PS_IMG_DIR_ . Configuration::get('PS_LOGO', null, null, $this->context->shop->id))) {
                    $logo = _PS_IMG_ . Configuration::get('PS_LOGO', null, null, $this->context->shop->id);
                } else {
                    $logo = '';
                }
            }
            $vars['{shop_logo}']          = $logo;
            $vars['{shop_name}']          = Tools::safeOutput(Configuration::get('PS_SHOP_NAME', null, null, $this->context->shop->id));
            $vars['{shop_url}']           = Context::getContext()->link->getPageLink('index', true, Context::getContext()->language->id, null, false, $this->context->shop->id);
            $vars['{my_account_url}']     = Context::getContext()->link->getPageLink('my-account', true, Context::getContext()->language->id, null, false, $this->context->shop->id);
            $vars['{guest_tracking_url}'] = Context::getContext()->link->getPageLink('guest-tracking', true, Context::getContext()->language->id, null, false, $this->context->shop->id);
            $vars['{history_url}']        = Context::getContext()->link->getPageLink('history', true, Context::getContext()->language->id, null, false, $this->context->shop->id);
            $vars['{color}']              = Tools::safeOutput(Configuration::get('PS_MAIL_COLOR', null, null, $this->context->shop->id));
            $mock_cr                      = new CartRule();
            $mock_cr->code                = 'abcdef';
            $mock_cr->free_shipping       = true;
            $mock_cr->reduction_percent   = 10;
            $tpl                          = $this->context->smarty->createTemplate($this->getLocalPath() . 'views/templates/hook/order_confirm.tpl');
            $tpl->assign('nl_cart_rule', $mock_cr);
            $vars['{content}'] = $tpl->fetch();
            $this->additionalMailFields($vars);
            $layout        = Tools::file_get_contents($this->getLocalPath() . 'mail_tpl/' . $layout . '/new_voucher.html');
            $layout        = str_replace(array_keys($vars), $vars, $layout);
            $this->content = $layout;
        }
        return $this->displayAjax();
    }
    public function additionalMailFields(&$template_vars)
    {
        $id_lang                               = Configuration::get('PS_LANG_DEFAULT');
        $template_vars['{new_products_url}']   = Context::getContext()->link->getPageLink('new-products', true, $id_lang);
        $template_vars['{prices_drop_url}']    = Context::getContext()->link->getPageLink('prices-drop', true, $id_lang);
        $template_vars['{best_sales_url}']     = Context::getContext()->link->getPageLink('best-sales', true, $id_lang);
        $template_vars['{stores_url}']         = Context::getContext()->link->getPageLink('stores', true, $id_lang);
        $template_vars['{contact_url}']        = Context::getContext()->link->getPageLink('contact', true, $id_lang);
        $template_vars['{sitemap_url}']        = Context::getContext()->link->getPageLink('sitemap', true, $id_lang);
        $template_vars['{order_slip_url}']     = Context::getContext()->link->getPageLink('order-slip', true, $id_lang);
        $template_vars['{addresses_url}']      = Context::getContext()->link->getPageLink('addresses', true, $id_lang);
        $template_vars['{identity_url}']       = Context::getContext()->link->getPageLink('identity', true, $id_lang);
        $template_vars['{shop_contact_email}'] = Configuration::get('PS_SHOP_EMAIL');
    }
    protected function _childValidation()
    {
        if (!Tools::getValue('use_amount') && !Tools::getValue('use_count')) {
            $this->errors[] = $this->l('You must select either depening on amount or count');
        }
        if (Tools::getValue('use_amount')) {
            if (!Tools::isSubmit('amount') || Tools::getValue('amount') == '' || !Tools::isSubmit('to_amount') || Tools::getValue('to_amount') == '') {
                $this->errors[] = $this->l('You must set cart amounts!');
            } elseif (Tools::getValue('amount') > Tools::getValue('to_amount')) {
                $this->errors[] = $this->l('"More than" cart amount  should be lower than "Less than" cart amount');
            }
        }
        if (Tools::getValue('use_count')) {
            if (!Tools::isSubmit('count') || Tools::getValue('count') == '' || !Tools::isSubmit('to_count') || Tools::getValue('to_count') == '') {
                $this->errors[] = $this->l('You must set product counts!');
            } elseif (Tools::getValue('count') > Tools::getValue('to_count')) {
                $this->errors[] = $this->l('"More than" products count  should be ' . 'lower than "Less than" products count ');
            }
        }
        if (Tools::isSubmit('gifts') && Tools::getValue('gifts') != '') {
            $gifts = explode('-', Tools::getValue('gifts'));
            array_pop($gifts);
            if (!count($gifts)) {
                $this->errors[] = $this->l('You should select at least one product for gift!');
            }
        }
    }
    protected function copyFromPost(&$object, $table)
    {
        parent::copyFromPost($object, $table);
        if (Tools::isSubmit('gifts') && Tools::getValue('gifts') != '') {
            $gifts = explode('-', Tools::getValue('gifts'));
            array_pop($gifts);
            $gifts         = array_unique($gifts);
            $object->gifts = array_map('intval', $gifts);
        }
    }
    private function getNLEnabledState()
    {
        if ($this->nl_enabled !== false) {
            return $this->nl_enabled;
        }
        $this->nl_enabled = (int) Configuration::get('spciw_nl_enabled');
        return $this->nl_enabled;
    }
}